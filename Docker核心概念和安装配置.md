<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
**Table of Contents**  *generated with [DocToc](https://github.com/thlorenz/doctoc)*

- [Docker核心概念和安装配置](#docker%E6%A0%B8%E5%BF%83%E6%A6%82%E5%BF%B5%E5%92%8C%E5%AE%89%E8%A3%85%E9%85%8D%E7%BD%AE)
  - [核心概念](#%E6%A0%B8%E5%BF%83%E6%A6%82%E5%BF%B5)
    - [Docker镜像](#docker%E9%95%9C%E5%83%8F)
    - [Docker容器](#docker%E5%AE%B9%E5%99%A8)
    - [Docker仓库](#docker%E4%BB%93%E5%BA%93)
  - [安装Docker引擎](#%E5%AE%89%E8%A3%85docker%E5%BC%95%E6%93%8E)
    - [Ubuntu环境下安装Docker](#ubuntu%E7%8E%AF%E5%A2%83%E4%B8%8B%E5%AE%89%E8%A3%85docker)
    - [CentOS环境下安装Docker](#centos%E7%8E%AF%E5%A2%83%E4%B8%8B%E5%AE%89%E8%A3%85docker)
      - [系统要求](#%E7%B3%BB%E7%BB%9F%E8%A6%81%E6%B1%82)
      - [安装Docker Engine-Community](#%E5%AE%89%E8%A3%85docker-engine-community)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

# Docker核心概念和安装配置

## 核心概念

Docker大部分的操作都围绕着它的三大核心概念：镜像、容器和仓库。因此，准确把握这三大核心概念对于掌握Docker技术尤为重要。

### Docker镜像

Docker镜像类似于虚拟机镜像，可以将它理解为一个只读的模板。

例如，一个镜像可以包含一个基本的操作系统环境，里面仅安装了Apache应用程序（或用户需要的其他软件）。可以把它称为一个Apache镜像。

镜像是创建Docker容器的基础。

通过版本管理和增量的文件系统，Docker提供了一套十分简单的机制来创建和更新现有的镜像，用户甚至可以从网上下载一个已经做好的应用镜像，并直接使用。

### Docker容器

Docker容器类似于一个轻量级的沙箱，Docker利用容器来运行和隔离应用。

容器是从镜像创建的应用运行实例。它可以启动、开始、停止、删除，而这些容器都是彼此相互隔离、互不可见的。

可以把容器看作一个简易版的Linux系统环境（包括root用户权限、进程空间、用户空间和网络空间等）以及运行在其中的应用程序打包而成的盒子。

> 注意：镜像自身是只读的。容器从镜像启动的时候，会在镜像的最上层创建一个可写层。

### Docker仓库

Docker仓库类似于代码仓库，是Docker集中存放镜像文件的场所。

有时候我们会将Docker仓库和仓库注册服务器（Registry）混为一谈，并不严格区分。实际上，仓库注册服务器是存放仓库的地方，其上往往存放着多个仓库。每个仓库集中存放某一类镜像，往往包括多个镜像文件，通过不同的标签（tag）来进行区分。例如存放Ubuntu操作系统镜像的仓库，被称为Ubuntu仓库，其中可能包括16.04、18.04等不同版本的镜像。仓库注册服务器的示例如图所示：

![注册服务器与仓库](Docker核心概念和安装配置/注册服务器与仓库.png)

根据所存储的镜像公开分享与否，Docker仓库可以分为公开仓库（Public）和私有仓库（Private）两种形式。

目前，最大的公开仓库是官方提供的Docker Hub，其中存放着数量庞大的镜像供用户下载。国内不少云服务提供商（如腾讯云、阿里云等）也提供了仓库的本地源，可以提供稳定的国内访问。

当然，用户如果不希望公开分享自己的镜像文件，Docker也支持用户在本地网络内创建一个只能自己访问的私有仓库。

当用户创建了自己的镜像之后就可以使用push命令将它上传到指定的公有或者私有仓库。这样用户下次在另外一台机器上使用该镜像时，只需要将其从仓库上pull下来就可以了。

> 可以看出，Docker利用仓库管理镜像的设计理念与Git代码仓库的概念非常相似，实际上Docker设计上借鉴了Git的很多优秀思想。

## 安装Docker引擎

Docker引擎是使用Docker容器的核心组件，可以在主流的操作系统和云平台上使用，包括Linux操作系统（如Ubuntu、Debian、CentOS、Redhat等）, macOS和Windows操作系统，以及IBM、亚马逊、微软等知名云平台。

用户可以访问Docker官网的[https://www.docker.com/get-docker](https://www.docker.com/get-docker)页面，查看获取Docker的方式，以及Docker支持的平台类型：

![获取docker](Docker核心概念和安装配置/获取docker.png)

目前Docker支持Docker引擎、Docker Hub、Docker Cloud等多种服务：

- Docker引擎：包括支持在桌面系统或云平台安装Docker，以及为企业提供简单安全弹性的容器集群编排和管理；
- DockerHub：官方提供的云托管服务，可以提供公有或私有的镜像仓库；
- DockerCloud：官方提供的容器云服务，可以完成容器的部署与管理，可以完整地支持容器化项目，还有CI、CD功能。

Docker引擎目前分为两个版本：社区版本（Community Edition，CE）和企业版本（Enterprise Edition，EE）。社区版本包括大部分的核心功能，企业版本则通过付费形式提供认证支持、镜像管理、容器托管、安全扫描等高级服务。通常情况下，用户使用社区版本可以满足大部分需求；若有更苛刻的需求，可以购买企业版本服务。社区版本每个月会发布一次尝鲜（Edge）版本，每个季度（3、6、9、12月）会发行一次稳定（Stable）版本。版本号命名格式为“年份.月份”，如2018年6月发布的版本号为v18.06。

推荐首选在Linux环境中使用Docker社区稳定版本，以获取最佳的原生支持体验。如无特殊说明，则以社区版本的稳定版为例进行说明。

### Ubuntu环境下安装Docker

**系统要求**

### CentOS环境下安装Docker

#### 系统要求

Docker目前支持CentOS 7及以后的版本。系统的要求跟Ubuntu情况类似，64位操作系统，内核版本至少为3.10：

```bash
[root@docker ~]# cat /etc/redhat-release 
CentOS Linux release 7.7.1908 (Core) // CentOS 版本
[root@docker ~]# uname -r
3.10.0-1062.el7.x86_64 // 说明内核版本为3.10
[root@docker ~]# hostname -I
192.168.217.225 
```

#### 安装Docker Engine-Community

**使用Docker仓库进行安装**

在新主机上首次安装Docker Engine-Community之前，需要设置Docker仓库。之后，您可以从仓库安装和更新Docker。

安装所需的软件包。yum-utils提供了yum-config-manager，并且device mapper存储驱动程序需要device-mapper-persistent-data和lvm2。

```bash
[root@docker yum.repos.d]# yum install -y yum-utils \
> device-mapper-persistent-data \
> lvm2

...

Dependency Updated:
  device-mapper.x86_64 7:1.02.158-2.el7_7.2                         device-mapper-event.x86_64 7:1.02.158-2.el7_7.2             
  device-mapper-event-libs.x86_64 7:1.02.158-2.el7_7.2              device-mapper-libs.x86_64 7:1.02.158-2.el7_7.2              
  lvm2-libs.x86_64 7:2.02.185-2.el7_7.2                            

Complete!
```

使用如下命令设置稳定的仓库：

```bash
[root@docker yum.repos.d]# yum-config-manager --add-repo https://download.docker.com/linux/centos/docker-ce.repo
Loaded plugins: fastestmirror
adding repo from: https://download.docker.com/linux/centos/docker-ce.repo
grabbing file https://download.docker.com/linux/centos/docker-ce.repo to /etc/yum.repos.d/docker-ce.repo
repo saved to /etc/yum.repos.d/docker-ce.repo
```

**安装Docker Engine-Community**

安装最新版本的Docker Engine-Community和containerd，或者转到下一步安装特定版本：

```bash
[root@docker yum.repos.d]# yum install docker-ce docker-ce-cli containerd-io
Loaded plugins: fastestmirror
Loading mirror speeds from cached hostfile
 * base: mirrors.aliyun.com
 * extras: mirrors.aliyun.com
 * updates: mirrors.cn99.com
No package containerd-io available.
Resolving Dependencies
...
Install  2 Packages (+9 Dependent packages)
...
Total download size: 89 M
Installed size: 369 M
Is this ok [y/d/N]: y
Downloading packages:
... 
Total                                                                                           408 kB/s |  89 MB  00:03:43     
Retrieving key from https://download.docker.com/linux/centos/gpg
Importing GPG key 0x621E9F35:
 Userid     : "Docker Release (CE rpm) <docker@docker.com>"
 Fingerprint: 060a 61c5 1b55 8a7f 742b 77aa c52f eb6b 621e 9f35
 From       : https://download.docker.com/linux/centos/gpg
Is this ok [y/N]: y // 请选接受GPG密钥。
Running transaction check
Running transaction test
Transaction test succeeded
...
Dependency Installed:
  audit-libs-python.x86_64 0:2.8.5-4.el7         checkpolicy.x86_64 0:2.5-8.el7     container-selinux.noarch 2:2.107-3.el7    
  containerd.io.x86_64 0:1.2.13-3.1.el7          libcgroup.x86_64 0:0.41-21.el7     libsemanage-python.x86_64 0:2.5-14.el7    
  policycoreutils-python.x86_64 0:2.5-33.el7     python-IPy.noarch 0:0.75-6.el7     setools-libs.x86_64 0:3.3.8-4.el7         
Complete!
```

> 有多个Docker仓库吗？如果启用了多个Docker仓库，则在未在yum install或yum update命令中指定版本的情况下，进行的安装或更新将始终安装最高版本，这可能不适合您的稳定性需求。

Docker安装完默认未启动。并且已经创建好docker用户组，但该用户组下没有用户。

**安装特定版本Docker Engine-Community**

1.列出并排序您存储库中可用的版本。

此示例按版本号（从高到低）对结果进行排序：

```bash
[root@docker yum.repos.d]# yum list docker-ce --showduplicates | sort -r
...
Installed Packages
 * extras: mirrors.aliyun.com
docker-ce.x86_64            3:19.03.7-3.el7                    docker-ce-stable 
docker-ce.x86_64            3:19.03.7-3.el7                    @docker-ce-stable
...
docker-ce.x86_64            17.03.0.ce-1.el7.centos            docker-ce-stable 
 * base: mirrors.aliyun.com
Available Packages
```

2.通过其完整的软件包名称安装特定版本，该软件包名称是软件包名称（docker-ce）加上版本字符串（第二列），从第一个冒号（:）一直到第一个连字符，并用连字符（-）分隔。例如：docker-ce-18.09.1。

```bash
[root@docker yum.repos.d]# sudo yum install docker-ce-<VERSION_STRING> docker-ce-cli-<VERSION_STRING> containerd.io
```

**启动Docker**

```bash
$ sudo systemctl start docker
```

通过运行hello-world镜像来验证是否正确安装了Docker Engine-Community：

```bash
$ sudo docker run hello-world
Hello from Docker!
This message shows that your installation appears to be working correctly.

To generate this message, Docker took the following steps:
 1. The Docker client contacted the Docker daemon.
 2. The Docker daemon pulled the "hello-world" image from the Docker Hub.
    (amd64)
 3. The Docker daemon created a new container from that image which runs the
    executable that produces the output you are currently reading.
 4. The Docker daemon streamed that output to the Docker client, which sent it
    to your terminal.

To try something more ambitious, you can run an Ubuntu container with:
 $ docker run -it ubuntu bash

Share images, automate workflows, and more with a free Docker ID:
 https://hub.docker.com/

For more examples and ideas, visit:
 https://docs.docker.com/get-started/
```