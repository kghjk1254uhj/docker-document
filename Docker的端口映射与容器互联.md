<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
**Table of Contents**  *generated with [DocToc](https://github.com/thlorenz/doctoc)*

- [Docker的端口映射与容器互联](#docker%E7%9A%84%E7%AB%AF%E5%8F%A3%E6%98%A0%E5%B0%84%E4%B8%8E%E5%AE%B9%E5%99%A8%E4%BA%92%E8%81%94)
  - [端口映射实现容器访问](#%E7%AB%AF%E5%8F%A3%E6%98%A0%E5%B0%84%E5%AE%9E%E7%8E%B0%E5%AE%B9%E5%99%A8%E8%AE%BF%E9%97%AE)
    - [从外部访问容器应用](#%E4%BB%8E%E5%A4%96%E9%83%A8%E8%AE%BF%E9%97%AE%E5%AE%B9%E5%99%A8%E5%BA%94%E7%94%A8)
    - [映射所有接口地址](#%E6%98%A0%E5%B0%84%E6%89%80%E6%9C%89%E6%8E%A5%E5%8F%A3%E5%9C%B0%E5%9D%80)
    - [映射到指定地址的指定端口](#%E6%98%A0%E5%B0%84%E5%88%B0%E6%8C%87%E5%AE%9A%E5%9C%B0%E5%9D%80%E7%9A%84%E6%8C%87%E5%AE%9A%E7%AB%AF%E5%8F%A3)
    - [映射到指定地址的任意端口](#%E6%98%A0%E5%B0%84%E5%88%B0%E6%8C%87%E5%AE%9A%E5%9C%B0%E5%9D%80%E7%9A%84%E4%BB%BB%E6%84%8F%E7%AB%AF%E5%8F%A3)
    - [查看映射端口配置](#%E6%9F%A5%E7%9C%8B%E6%98%A0%E5%B0%84%E7%AB%AF%E5%8F%A3%E9%85%8D%E7%BD%AE)
  - [互联机制实现便捷互访](#%E4%BA%92%E8%81%94%E6%9C%BA%E5%88%B6%E5%AE%9E%E7%8E%B0%E4%BE%BF%E6%8D%B7%E4%BA%92%E8%AE%BF)
    - [自定义容器命名](#%E8%87%AA%E5%AE%9A%E4%B9%89%E5%AE%B9%E5%99%A8%E5%91%BD%E5%90%8D)
    - [容器互联](#%E5%AE%B9%E5%99%A8%E4%BA%92%E8%81%94)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

# Docker的端口映射与容器互联

## 端口映射实现容器访问

### 从外部访问容器应用

在启动容器的时候，如果不指定对应参数，在容器外部是无法通过网络来访问容器内的网络应用和服务的。

当容器中运行一些网络应用，要让外部访问这些应用时，可以通过-P或-p参数来指定端口映射。当使用-P（大写的）标记时，Docker会随机映射一个49000～49900的端口到内部容器开放的网络端口：

```bash
[root@docker ~]# docker run -d -P --name nginx_1 nginx:latest
c3eebbe18c8f47c13819261d925198c5b6b61bb742bc21f4869b82d09bdb9a34
[root@docker ~]# docker ps -l
CONTAINER ID        IMAGE               COMMAND                  CREATED             STATUS              PORTS                   NAMES
c3eebbe18c8f        nginx:latest        "nginx -g 'daemon of…"   6 seconds ago       Up 5 seconds        0.0.0.0:32768->80/tcp   nginx_1
```

此时，可以使用docker ps看到，本地主机的32768被映射到了容器的80端口。访问宿主主机的32768端口即可访问容器内web应用提供的界面。

![外部访问容器页面验证](Docker的端口映射与容器互联/外部访问容器页面验证.png)

同样可以通过docker logs命令查看应用信息：

```bash
[root@docker ~]# docker logs nginx_1
192.168.0.2 - - [12/Mar/2020:06:57:31 +0000] "GET / HTTP/1.1" 200 612 "-" "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.132 Safari/537.36" "-"
2020/03/12 06:57:31 [error] 6#6: *1 open() "/usr/share/nginx/html/favicon.ico" failed (2: No such file or directory), client: 192.168.0.2, server: localhost, request: "GET /favicon.ico HTTP/1.1", host: "192.168.0.217:32768", referrer: "http://192.168.0.217:32768/"
192.168.0.2 - - [12/Mar/2020:06:57:31 +0000] "GET /favicon.ico HTTP/1.1" 404 555 "http://192.168.0.217:32768/" "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.132 Safari/537.36" "-"
```

-p(小写p)可以指定要映射的端口，并且在一个指定的端口上只可以绑定一个容器。

支持的格式有：

```bash
IP:HostPort:ContainerPort|IP::ContainerPort|HostPort:ContainerPort
```

### 映射所有接口地址

使用HostPort:ContainerPort格式将本地的5000端口映射到容器的5000端口：

```bash
[root@docker ~]# docker run -itd -p 5000:5000 --name nginx_2 nginx:latest
82df2ee203851e53ca9debb2b020451bfa7ced0b1226e99d1636eb1bd2fb941a
[root@docker ~]# docker ps
CONTAINER ID        IMAGE               COMMAND                  CREATED             STATUS              PORTS                            NAMES
82df2ee20385        nginx:latest        "nginx -g 'daemon of…"   6 seconds ago       Up 4 seconds        80/tcp, 0.0.0.0:5000->5000/tcp   nginx_2
```

此时默认会绑定本地所有接口上的所有地址。多次使用-p参数可以绑定多个端口：

```bash
[root@docker ~]# docker run -itd -p 2000:2700 -p 2389:8863 --name nginx_3 nginx:latest
32ead6fe6cce08452ae0071c6eea6616e8f4a62cd70cec74c87f911172449257
[root@docker ~]# docker ps
CONTAINER ID        IMAGE               COMMAND                  CREATED              STATUS              PORTS                                                    NAMES
32ead6fe6cce        nginx:latest        "nginx -g 'daemon of…"   3 seconds ago        Up 1 second         80/tcp, 0.0.0.0:2000->2700/tcp, 0.0.0.0:2389->8863/tcp   nginx_3
```

### 映射到指定地址的指定端口

可以使用IP:HostPort:ContainerPort格式指定映射使用一个特定地址：

```bash
[root@docker ~]# docker run -itd -p 192.168.0.217:89:8081 --name nginx_4 nginx:latest
40346d3c72d2a09bd621bcbc1b7e95dee7e534b0dbe745071c24c0be66c04365
[root@docker ~]# docker ps
CONTAINER ID        IMAGE               COMMAND                  CREATED              STATUS              PORTS                                                    NAMES
40346d3c72d2        nginx:latest        "nginx -g 'daemon of…"   About a minute ago   Up About a minute   80/tcp, 192.168.0.217:89->8081/tcp                       nginx_4
```

### 映射到指定地址的任意端口

使用IP::ContainerPort绑定本机的任意端口到容器的8082端口，本地主机会自动分配一个端口：

```bash
[root@docker ~]# docker run -itd -p 192.168.0.217::8082 --name nginx_5 nginx:latest
e05da40bbd5969c2575e8620792e736ebe3f6e859de80dd50e8b6e13c6b2f8b1
[root@docker ~]# docker ps
CONTAINER ID        IMAGE               COMMAND                  CREATED             STATUS              PORTS                                                    NAMES
e05da40bbd59        nginx:latest        "nginx -g 'daemon of…"   6 seconds ago       Up 4 seconds        80/tcp, 192.168.0.217:32769->8082/tcp                    nginx_5
```

容器启动后，本机会随机自动分配一个未被占用的端口。

### 查看映射端口配置

使用docker port命令来查看当前映射的端口配置，也可以查看绑定的地址：

```bash
[root@docker ~]# docker port nginx_1
80/tcp -> 0.0.0.0:32768
[root@docker ~]# docker port nginx_2
5000/tcp -> 0.0.0.0:5000
[root@docker ~]# docker port nginx_3
2700/tcp -> 0.0.0.0:2000
8863/tcp -> 0.0.0.0:2389
[root@docker ~]# docker port nginx_4
8081/tcp -> 192.168.0.217:89
[root@docker ~]# docker port nginx_5
8082/tcp -> 192.168.0.217:32769
```

> 注意：容器有自己的内部网络和IP地址，使用docker \[container] inspect+容器ID可以获取容器的具体信息。

## 互联机制实现便捷互访

容器的互联（linking）是一种让多个容器中的应用进行快速交互的方式。它会在源和接收容器之间创建连接关系，接收容器可以通过容器名快速访问到源容器，而不用指定具体的IP地址。

### 自定义容器命名

连接系统依据容器的名称来执行。因此，首先需要自定义一个好记的容器命名。虽然当创建容器的时候，系统默认会分配一个名字，但自定义命名容器有两个好处：

- 自定义的命名，比较好记，比如一个Web应用容器我们可以给它起名叫web，一目了然
- 当要连接其他容器时候（即便重启），也可以使用容器名而不用改变，比如连接web容器到db容器

使用--name标记可以为容器自定义命名：

```bash
[root@docker ~]# docker run -itd --name centos_1 centos:latest
ac3dcf4d7fd9605966e70e0a376fb81888a4cae0531bcc3a79cb8fa4267b47b3
[root@docker ~]# docker ps
CONTAINER ID        IMAGE               COMMAND                  CREATED             STATUS              PORTS                                                    NAMES
ac3dcf4d7fd9        centos:latest       "/bin/bash"              2 minutes ago       Up 2 minutes                                                                 centos_1
e05da40bbd59        nginx:latest        "nginx -g 'daemon of…"   15 minutes ago      Up About a minute   80/tcp, 192.168.0.217:32770->8082/tcp                    nginx_5
40346d3c72d2        nginx:latest        "nginx -g 'daemon of…"   22 minutes ago      Up About a minute   80/tcp, 192.168.0.217:89->8081/tcp                       nginx_4
32ead6fe6cce        nginx:latest        "nginx -g 'daemon of…"   26 minutes ago      Up About a minute   80/tcp, 0.0.0.0:2000->2700/tcp, 0.0.0.0:2389->8863/tcp   nginx_3
82df2ee20385        nginx:latest        "nginx -g 'daemon of…"   27 minutes ago      Up About a minute   80/tcp, 0.0.0.0:5000->5000/tcp                           nginx_2
c3eebbe18c8f        nginx:latest        "nginx -g 'daemon of…"   45 minutes ago      Up About a minute   0.0.0.0:32769->80/tcp                                    nginx_1
```

通过docker ps或者docker ps -a可以查看到容器的自定义名字，利用docker inspect也可以获取到容器自定义名字：

```bash
[root@docker ~]# docker inspect -f ".{{.Name}}" ac3dcf4d7fd9
./centos_1
```

> 注意：容器的名称是唯一的。如果已经命名了一个web的容器，当再次使用web这个命名的时候会报错，如果一定要使用，需要先用docker rm删除之前创建的web容器。

在执行docker run的时候如果添加--rm参数，则容器终止后会立刻删除。--rm参数和-d参数不能同时使用。

### 容器互联

使用--link参数可以让容器之间安全地进行交互。

下面先创建一个新的数据库容器：

```bash
[root@docker ~]# docker run -itd --name mysql --env MYSQL_ROOT_PASSWORD=root mysql:5.7
14061fc9fdce571fe590816f197392ed7b5bf934a36e6e64a59f519b5b764372
[root@docker ~]# docker ps
CONTAINER ID        IMAGE               COMMAND                  CREATED             STATUS              PORTS                                                    NAMES
14061fc9fdce        mysql:5.7           "docker-entrypoint.s…"   5 seconds ago       Up 4 seconds        3306/tcp, 33060/tcp                                      mysql
```

创建一个web容器并将它连接到mysql容器：

```bash
[root@docker ~]# docker run -d -P --name web --link mysql:mysql nginx:latest
f55dd4ed7b0a6308fc237eacbd3bd831fe6c184366ac85561e4dfc44a13461a9
```

此时，db容器和web容器建立互联关系。

--link参数的格式为--link name:alias，其中name是要链接的容器的名称，alias是别名。

使用docker ps来查看容器的连接：

```bash
[root@docker ~]# docker ps
CONTAINER ID        IMAGE               COMMAND                  CREATED             STATUS              PORTS                                                    NAMES
f55dd4ed7b0a        nginx:latest        "nginx -g 'daemon of…"   4 seconds ago       Up 3 seconds        0.0.0.0:32772->80/tcp                                    web
14061fc9fdce        mysql:5.7           "docker-entrypoint.s…"   14 minutes ago      Up 14 minutes       3306/tcp, 33060/tcp                                      mysql
```

假如可以看到自定义命名的容器：mysql和web，mysql容器的names列有mysql也有web/mysql。这表示web容器链接到mysql容器，web容器将被允许访问mysql容器的信息。

Docker相当于在两个互联的容器之间创建了一个虚机通道，而且不用映射它们的端口到宿主主机上。在启动db容器的时候并没有使用-p和-P标记，从而避免了暴露数据库服务端口到外部网络上。

Docker通过两种方式为容器公开连接信息：

- 更新环境变量；
- 更新/etc/hosts文件。

使用env命令来查看web容器的环境变量：

```bash
[root@docker ~]# docker run --rm --name web2 --link mysql:mysql nginx:latest env
PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin
HOSTNAME=62fd287382e0
MYSQL_PORT=tcp://172.17.0.8:3306
MYSQL_PORT_3306_TCP=tcp://172.17.0.8:3306
MYSQL_PORT_3306_TCP_ADDR=172.17.0.8
MYSQL_PORT_3306_TCP_PORT=3306
MYSQL_PORT_3306_TCP_PROTO=tcp
MYSQL_PORT_33060_TCP=tcp://172.17.0.8:33060
MYSQL_PORT_33060_TCP_ADDR=172.17.0.8
MYSQL_PORT_33060_TCP_PORT=33060
MYSQL_PORT_33060_TCP_PROTO=tcp
MYSQL_NAME=/web2/mysql
MYSQL_ENV_MYSQL_ROOT_PASSWORD=root
MYSQL_ENV_GOSU_VERSION=1.7
MYSQL_ENV_MYSQL_MAJOR=5.7
MYSQL_ENV_MYSQL_VERSION=5.7.29-1debian10
NGINX_VERSION=1.17.9
NJS_VERSION=0.3.9
PKG_RELEASE=1~buster
HOME=/root
```

其中DB_开头的环境变量是供web容器连接db容器使用，前缀采用大写的连接别名。

除了环境变量，Docker还添加host信息到父容器的/etc/hosts的文件。下面是父容器web的hosts文件：

```bash
[root@docker ~]# docker run -it --rm --link mysql:mysql nginx:latest /bin/bash
root@b454a0fa3399:/# cat /etc/hosts
127.0.0.1       localhost
::1     localhost ip6-localhost ip6-loopback
fe00::0 ip6-localnet
ff00::0 ip6-mcastprefix
ff02::1 ip6-allnodes
ff02::2 ip6-allrouters
172.17.0.8      mysql 14061fc9fdce
172.17.0.10     b454a0fa3399
```

这里有2个hosts信息，第一个是web容器，web容器用自己的id作为默认主机名，第二个是db容器的IP和主机名。

可以在web容器中安装ping命令来测试跟mysql容器的连通：

```bash
root@b454a0fa3399:/# apt-get update && apt-get install iputils-ping
root@b454a0fa3399:/# ping mysql
PING mysql (172.17.0.8) 56(84) bytes of data.
64 bytes from mysql (172.17.0.8): icmp_seq=1 ttl=64 time=0.072 ms
64 bytes from mysql (172.17.0.8): icmp_seq=2 ttl=64 time=0.129 ms
64 bytes from mysql (172.17.0.8): icmp_seq=3 ttl=64 time=0.132 ms
```

用ping来测试db容器，它会解析成172.17.0.8。

用户可以链接多个子容器到父容器，比如可以链接多个web到同一个db容器上。