<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
**Table of Contents**  *generated with [DocToc](https://github.com/thlorenz/doctoc)*

- [Docker数据管理](#docker%E6%95%B0%E6%8D%AE%E7%AE%A1%E7%90%86)
  - [数据卷](#%E6%95%B0%E6%8D%AE%E5%8D%B7)
    - [创建容器卷](#%E5%88%9B%E5%BB%BA%E5%AE%B9%E5%99%A8%E5%8D%B7)
    - [绑定数据卷](#%E7%BB%91%E5%AE%9A%E6%95%B0%E6%8D%AE%E5%8D%B7)
  - [数据卷容器](#%E6%95%B0%E6%8D%AE%E5%8D%B7%E5%AE%B9%E5%99%A8)
  - [利用数据卷容器来迁移数据](#%E5%88%A9%E7%94%A8%E6%95%B0%E6%8D%AE%E5%8D%B7%E5%AE%B9%E5%99%A8%E6%9D%A5%E8%BF%81%E7%A7%BB%E6%95%B0%E6%8D%AE)
    - [备份](#%E5%A4%87%E4%BB%BD)
    - [恢复](#%E6%81%A2%E5%A4%8D)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

# Docker数据管理

在生产环境中使用Docker，往往需要对数据进行持久化，或者需要在多个容器之间进行数据共享，这必然涉及容器的数据管理操作。

容器中的管理数据主要有两种方式：

- 数据卷（Data Volumes）：容器内数据直接映射到本地主机环境；
- 数据卷容器（Data Volume Containers）：使用特定容器维护数据卷。

此处将介绍如何在容器内创建数据卷，并且把本地的目录或文件挂载到容器内的数据卷中。接下来，介绍如何使用数据卷容器在容器和主机、容器和容器之间共享数据，并实现数据的备份和恢复。

## 数据卷

数据卷（Data Volumes）是一个可供容器使用的特殊目录，它将主机操作系统目录直接映射进容器，类似于Linux中的mount行为。

数据卷可以提供很多有用的特性：

- 数据卷可以在容器之间共享和重用，容器间传递数据将变得高效与方便；
- 对数据卷内数据的修改会立马生效，无论是容器内操作还是本地操作；
- 对数据卷的更新不会影响镜像，解耦开应用和数据；
- 卷会一直存在，直到没有容器使用，可以安全地卸载它。

### 创建容器卷

Docker提供了volume子命令来管理数据卷，如下命令可以快速在本地创建一个数据卷：

```bash
[root@docker ~]# docker volume create -d local friendly_gagarin
friendly_gagarin
```

此时，查看/var/lib/docker/volumes路径下，会发现所创建的数据卷位置：

```bash
[root@docker ~]# ls -l /var/lib/docker/volumes
total 24
drwxr-xr-x. 3 root root    19 Mar 11 05:09 friendly_gagarin
-rw-------. 1 root root 32768 Mar 11 05:09 metadata.db
```

除了create子命令外，docker volume还支持inspect（查看详细信息）、ls（列出已有数据卷）、prune（清理无用数据卷）、rm（删除数据卷）等。

### 绑定数据卷

除了使用volume子命令来管理数据卷外，还可以在创建容器时将主机本地的任意路径挂载到容器内作为数据卷，这种形式创建的数据卷称为绑定数据卷。

在用docker \[container] run命令的时候，可以使用-mount选项来使用数据卷。

-mount选项支持三种类型的数据卷，包括：

- volume：普通数据卷，映射到主机/var/lib/docker/volumes路径下；
- bind：绑定数据卷，映射到主机指定路径下；
- tmpfs：临时数据卷，只存在于内存中。

下面使用ubuntu:14.04镜像创建一个容器，并创建一个数据卷挂载到容器的/opt目录：

```bash
[root@docker ~]# docker run -it -d -P --name ubuntu --mount type=bind,source=/opt,target=/opt ubuntu:14.04 /bin/bash
5a1c64dc7dc574b8c4f1a4e1dba7dd27a1ce8b249ef8194fc5495497919abe87
```

此时，我们进入到容器的opt目录下：

```bash
[root@docker ~]# docker exec -it 5a1c64dc7dc5 /bin/bash
root@5a1c64dc7dc5:/# ls
bin  boot  dev  etc  fastboot  home  lib  lib64  lost+found  media  mnt  opt  proc  root  run  sbin  srv  sys  tmp  usr  var
root@5a1c64dc7dc5:/# cd /opt/
root@5a1c64dc7dc5:/opt# ll
total 0
drwxr-xr-x. 4 root root 38 Mar 10 03:36 ./
drwxr-xr-x. 1 root root  6 Mar 11 08:10 ../
drwx--x--x. 4 root root 28 Mar  9 05:23 containerd/
drwxr-xr-x. 2 root root 66 Mar 10 05:31 docker/
```

发现下面的内容和宿主机的opt下的内容一致，这时候在宿主机创建1.txt，内容为：

```bash
Volume test
```

然后查看容器中对应目录下的1.txt：

```bash
root@5a1c64dc7dc5:/opt# cat 1.txt 
Volume test
```

上述挂载的命令相当于可以使用-v标记可以在容器中创建一个数据卷：

```bash
[root@docker ~]# docker run -it -d -P --name ubuntu -v /opt:/opt ubuntu:14.04 /bin/bash
df980d7850d47f9960696f4bad92c58f6ba2f8dd57933097fe816d255c7a7f62
[root@docker ~]# docker exec -it df980d78 /bin/bash
root@df980d7850d4:/# cd /opt/
root@df980d7850d4:/opt# ll
total 4
drwxr-xr-x. 4 root root 51 Mar 11 08:23 ./
drwxr-xr-x. 1 root root  6 Mar 11 08:29 ../
-rw-r--r--. 1 root root 12 Mar 11 08:23 1.txt
drwx--x--x. 4 root root 28 Mar  9 05:23 containerd/
drwxr-xr-x. 2 root root 66 Mar 10 05:31 docker/
root@df980d7850d4:/opt# 
```

这个功能在进行应用测试的时候十分方便，比如用户可以放置一些程序或数据到本地目录中实时进行更新，然后在容器内运行和使用。

另外，本地目录的路径必须是绝对路径，容器内路径可以为相对路径。如果目录不存在，Docker会自动创建。

Docker挂载数据卷的默认权限是读写（rw），用户也可以通过ro指定为只读：

```bash
[root@docker ~]# docker run -it -d -P --name ubuntu -v /opt:/opt:ro ubuntu:14.04 /bin/bash
df980d7850d47f9960696f4bad92c58f6ba2f8dd57933097fe816d255c7a7f62
```

加了：ro之后，容器内对所挂载数据卷内的数据就无法修改了。如果直接挂载一个文件到容器，使用文件编辑工具，包括vi或者sed --in-place的时候，可能会造成文件inode的改变。从Docker 1.1.0起，这会导致报错误信息。所以推荐的方式是直接挂载文件所在的目录到容器内。

## 数据卷容器

如果用户需要在多个容器之间共享一些持续更新的数据，最简单的方式是使用数据卷容器。数据卷容器也是一个容器，但是它的目的是专门提供数据卷给其他容器挂载。

首先，创建一个数据卷容器dbdata，并在其中创建一个数据卷挂载到/dbdata：

```bash
[root@docker opt]# docker run -it -v /dbdata --name dbdata ubuntu:18.04
```

查看/dbdata目录：

```bash
root@d071817992bf:/# ls
bin  boot  dbdata  dev  etc  home  lib  lib64  media  mnt  opt  proc  root  run  sbin  srv  sys  tmp  usr  var
```

然后，可以在其他容器中使用--volumes-from来挂载dbdata容器中的数据卷，例如创建db1和db2两个容器，并从dbdata容器挂载数据卷：

```bash
$ docker run -it --volumes-from dbdata --name db1 ubuntu:18.04
root@a5a66e096c71:/#
$ docker run -it --volumes-from dbdata --name db2 ubuntu:18.04
root@5580aa5aff75:/#
```

此时，容器db1和db2都挂载同一个数据卷到相同的/dbdata目录，三个容器任何一方在该目录下的写入，其他容器都可以看到。

例如，在dbdata容器中创建一个test文件：

```bash
root@d071817992bf:/dbdata# cd /dbdata
root@d071817992bf:/dbdata# touch test
```

在db1容器内查看它：

```bash
root@a5a66e096c71:/dbdata# ls
test
```

可以多次使用--volumes-from参数来从多个容器挂载多个数据卷，还可以从其他已经挂载了容器卷的容器来挂载数据卷：

```bash
[root@docker ~]# docker run -d --name db3 --volumes-from db1 ubuntu:18.04
4fc2c794d5132da5b7263863b03f902f8f735000054d406f65a2ae6f71378404
```

> 注意：使用--volumes-from参数所挂载数据卷的容器自身并不需要保持在运行状态。

如果删除了挂载的容器（包括dbdata、db1和db2），数据卷并不会被自动删除。如果要删除一个数据卷，必须在删除最后一个还挂载着它的容器时显式使用docker rm -v命令来指定同时删除关联的容器。

使用数据卷容器可以让用户在容器之间自由地升级和移动数据卷，具体的操作将在下一节进行讲解。

## 利用数据卷容器来迁移数据

可以利用数据卷容器对其中的数据卷进行备份、恢复，以实现数据的迁移。

### 备份

使用下面的命令来备份dbdata数据卷容器内的数据卷：

```bash
[root@docker ~]# docker run --volumes-from dbdata -v $(pwd):/backup --name worker ubuntu:18.04 tar cvf /backup/backup.tar.gz /dbdata
tar: Removing leading `/' from member names
/dbdata/
/dbdata/test
```

具体分析上面的命令：

- 1.使用ubuntu:18.04镜像创建了一个名为worker的容器
- 2.使用--volumes-from dbdata参数来让worker容器挂载dbdata容器的数据卷，也就是dbdata数据卷
- 3.使用-v $(pwd):/backup参数来挂载本地的当前目录到worker容器的/backup目录
- 4.worker容器启动后，使用tar cvf /backup/backup.tar.gz /dbdata命令将/dbdata下内容备份为容器内的/backup/backup.tar.gz，即宿主主机当前目录下的backup.tar.gz。

### 恢复

如果要恢复数据到一个容器，可以按照下面的操作。

首先创建一个带有数据卷的容器dbdata2：

```bash
[root@docker ~]# docker run -v /dbdata --name dbdata2 ubuntu:18.04 /bin/bash
[root@docker ~]# docker ps -a
CONTAINER ID        IMAGE               COMMAND                  CREATED             STATUS                      PORTS               NAMES
41ddfd683d13        ubuntu:18.04        "/bin/bash"              14 seconds ago      Exited (0) 12 seconds ago                       dbdata2
```

然后创建另一个新的容器，挂载dbdata2的容器，并使用untar解压备份文件到所挂在的容器卷中：

```bash
[root@docker ~]# docker run --volumes-from dbdata2 -v $(pwd):/backup ubuntu:18.04 tar xvf /backup/backup.tar.gz
dbdata/
dbdata/test
```