<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
**Table of Contents**  *generated with [DocToc](https://github.com/thlorenz/doctoc)*

- [Docker大数据容器集群使用文档](#docker%E5%A4%A7%E6%95%B0%E6%8D%AE%E5%AE%B9%E5%99%A8%E9%9B%86%E7%BE%A4%E4%BD%BF%E7%94%A8%E6%96%87%E6%A1%A3)
    - [写在前面](#%E5%86%99%E5%9C%A8%E5%89%8D%E9%9D%A2)
  - [普通用户使用流程（非root用户）且创建的子网为192.168.218.xxx](#%E6%99%AE%E9%80%9A%E7%94%A8%E6%88%B7%E4%BD%BF%E7%94%A8%E6%B5%81%E7%A8%8B%E9%9D%9Eroot%E7%94%A8%E6%88%B7%E4%B8%94%E5%88%9B%E5%BB%BA%E7%9A%84%E5%AD%90%E7%BD%91%E4%B8%BA192168218xxx)
    - [自定义网络](#%E8%87%AA%E5%AE%9A%E4%B9%89%E7%BD%91%E7%BB%9C)
    - [pull并重命名已经配置互信的对应的images](#pull%E5%B9%B6%E9%87%8D%E5%91%BD%E5%90%8D%E5%B7%B2%E7%BB%8F%E9%85%8D%E7%BD%AE%E4%BA%92%E4%BF%A1%E7%9A%84%E5%AF%B9%E5%BA%94%E7%9A%84images)
    - [进入对应文件夹，执行脚本文件](#%E8%BF%9B%E5%85%A5%E5%AF%B9%E5%BA%94%E6%96%87%E4%BB%B6%E5%A4%B9%E6%89%A7%E8%A1%8C%E8%84%9A%E6%9C%AC%E6%96%87%E4%BB%B6)
    - [创建容器命令](#%E5%88%9B%E5%BB%BA%E5%AE%B9%E5%99%A8%E5%91%BD%E4%BB%A4)
    - [分别打开三个shell窗口进入容器的命令](#%E5%88%86%E5%88%AB%E6%89%93%E5%BC%80%E4%B8%89%E4%B8%AAshell%E7%AA%97%E5%8F%A3%E8%BF%9B%E5%85%A5%E5%AE%B9%E5%99%A8%E7%9A%84%E5%91%BD%E4%BB%A4)
    - [hadoop初始化](#hadoop%E5%88%9D%E5%A7%8B%E5%8C%96)
    - [分别到各个容器内启动ZK的组件服务](#%E5%88%86%E5%88%AB%E5%88%B0%E5%90%84%E4%B8%AA%E5%AE%B9%E5%99%A8%E5%86%85%E5%90%AF%E5%8A%A8zk%E7%9A%84%E7%BB%84%E4%BB%B6%E6%9C%8D%E5%8A%A1)
    - [在master容器内开启Haodoop组件服务](#%E5%9C%A8master%E5%AE%B9%E5%99%A8%E5%86%85%E5%BC%80%E5%90%AFhaodoop%E7%BB%84%E4%BB%B6%E6%9C%8D%E5%8A%A1)
    - [在master容器内开启Spark组件服务](#%E5%9C%A8master%E5%AE%B9%E5%99%A8%E5%86%85%E5%BC%80%E5%90%AFspark%E7%BB%84%E4%BB%B6%E6%9C%8D%E5%8A%A1)
    - [上述服务启动起来之后的结果](#%E4%B8%8A%E8%BF%B0%E6%9C%8D%E5%8A%A1%E5%90%AF%E5%8A%A8%E8%B5%B7%E6%9D%A5%E4%B9%8B%E5%90%8E%E7%9A%84%E7%BB%93%E6%9E%9C)
    - [写在最后](#%E5%86%99%E5%9C%A8%E6%9C%80%E5%90%8E)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

# Docker大数据容器集群使用文档

前提：已经将软件包拷贝到自己的服务器目录下，比如这边是拷贝到：

```bash
huangzp@sea-Precision-5820-Tower-X-Series:/data/huangzp/docker$ pwd
/data/huangzp/docker
```

并且，在构建过程中，所有的组件对应的软件都是安装在/usr/local/目录下，并且已经去掉版本号了，比如解压出来的hadoop-2.8.5目录名称已经改为hadoop目录。

### 写在前面

这边建议一台服务器创建一个容器集群就好，否则需要修改的东西都比较多，因为Spark中涉及到一些配置需要修改IP，这时候最好不使用和上面一样的同一个子网，并且还需要修改hostname，并重新构建互信的基础镜像，因为互信基础镜像是基于hostname为master，slave1，slave2的基础上\[但不涉及IP]。

因此，建议按照下面的流程走，如果是root用户，则搭建过程将更加简化。

## 普通用户使用流程（非root用户）且创建的子网为192.168.218.xxx

比如这边使用的是huangzp普通用户，因此执行docker命令的时候都需要加上sudo。

### 自定义网络

```bash
$ sudo docker network create --subnet=192.168.218.221/24 mynetwork
$ sudo docker network ls
NETWORK ID          NAME                DRIVER              SCOPE
20d4dd3ee621        bridge              bridge              local
4f31fb4c4d02        host                host                local
006c3401568f        mynetwork           bridge              local  // 出现NAME为mynetwork，说明创建成功
55e4a7003b8e        none                null                local
```

### pull并重命名已经配置互信的对应的images

接下来时拉取重新标志镜像：

```bash
$ sudo docker pull hchihping/centos:master_v1.0
$ sudo docker pull hchihping/centos:slave1_v1.0
$ sudo docker pull hchihping/centos:slave2_v1.0
```

然后重新命名镜像：

```bash
$ sudo docker tag hchihping/centos:master_v1.0 centos:master_v1.0
$ sudo docker tag hchihping/centos:slave1_v1.0 centos:slave1_v1.0
$ sudo docker tag hchihping/centos:slave2_v1.0 centos:slave2_v1.0
```

### 进入对应文件夹，执行脚本文件

进入对应的文件夹，执行命令。

1.进入master文件夹：

```bash
# 将软件包拷贝到master目录下
$ cd master && cp -r ../cluster_soft/ ./

# 构建镜像
$ sudo docker image build -t centos:master_v1.3 .

# 镜像构建成功
huangzp@sea-Precision-5820-Tower-X-Series:/data/huangzp/docker$ sudo docker images
[sudo] password for huangzp:
REPOSITORY             TAG                 IMAGE ID            CREATED             SIZE
centos                 slave2_v1.3         1ae74091f599        About an hour ago   2.62GB
centos                 slave1_v1.3         3e8ab3ffb87c        About an hour ago   2.62GB
centos                 master_v1.3         d5132657c96d        About an hour ago   2.62GB
```

2.进入slave1文件夹：

```bash
# 将软件包拷贝到slave1目录下
$ cd slave1 && cp -r ../cluster_soft/ ./

# 构建镜像
$ sudo docker image build -t centos:slave1_v1.3 .

# 镜像构建成功
huangzp@sea-Precision-5820-Tower-X-Series:/data/huangzp/docker$ sudo docker images
huangzp@sea-Precision-5820-Tower-X-Series:/data/huangzp/docker$ sudo docker images
[sudo] password for huangzp:
REPOSITORY             TAG                 IMAGE ID            CREATED             SIZE
centos                 slave1_v1.3         3e8ab3ffb87c        About an hour ago   2.62GB
```

3.进入slave2文件夹：

```bash
# 将软件包拷贝到slave2目录下
$ cd slave2 && cp -r ../cluster_soft/ ./

# 构建镜像
$ sudo docker image build -t centos:slave2_v1.3 .

# 镜像构建成功
huangzp@sea-Precision-5820-Tower-X-Series:/data/huangzp/docker$ sudo docker images
[sudo] password for huangzp:
REPOSITORY             TAG                 IMAGE ID            CREATED             SIZE
centos                 slave2_v1.3         1ae74091f599        About an hour ago   2.62GB
```

### 创建容器命令

这时候我们的镜像已经构建成功了，这时候复制打开三个shell窗口以根据不同的镜像创建容器，并且进入容器。

1.master创建命令如下：

```bash
[root@docker docker]# docker run -itd --name master -h master -p ::50070 -p ::50075 -p ::50475 -p ::9000 -p ::49001 -p ::19888 -p ::8032 -p ::8030 -p ::8088 -p ::8090 -p ::8031 -p ::8033 -p ::8080 -p ::8081 -p ::2181 --add-host master:192.168.218.221 --add-host slave1:192.168.218.222 --add-host slave2:192.168.218.223 --net=mynetwork --ip=192.168.218.221 centos:master_v1.3
```

2.slave1创建命令如下：

```bash
[root@docker docker]# docker run -itd --name slave1 -h slave1 -p ::2181 -p ::8042 --add-host master:192.168.218.221 --add-host slave1:192.168.218.222 --add-host slave2:192.168.218.223 --net=mynetwork --ip=192.168.218.222 centos:slave1_v1.3 && docker exec -it slave1 /bin/bash
```

3.slave2创建命令如下：

```bash
[root@docker docker]# docker run -itd --name slave2 -h slave2 -p ::2181 -p ::8042 --add-host master:192.168.218.221 --add-host slave1:192.168.218.222 --add-host slave2:192.168.218.223 --net=mynetwork --ip=192.168.218.223 centos:slave2_v1.3 && docker exec -it slave2 /bin/bash
```

此时重新打开一个窗口，我们可以看到容器已经创建成功，并且有些端口已经映射成功了：

```bash
huangzp@sea-Precision-5820-Tower-X-Series:/data/huangzp/docker$ sudo docker ps
CONTAINER ID        IMAGE                COMMAND                  CREATED             STATUS              PORTS                                                                                                                                                                                                                                                                                                                                                                                        NAMES
73b4eb4d7953        centos:slave2_v1.3   "sh -c '/usr/sbin/ss…"   About an hour ago   Up About an hour    0.0.0.0:32786->2181/tcp, 0.0.0.0:32785->8042/tcp                                                                                                                                                                                                                                                                                                                                             slave2
addbe197bbe6        centos:slave1_v1.3   "sh -c '/usr/sbin/ss…"   About an hour ago   Up About an hour    0.0.0.0:32784->2181/tcp, 0.0.0.0:32783->8042/tcp                                                                                                                                                                                                                                                                                                                                             slave1
5bde6150c342        centos:master_v1.3   "sh -c '/usr/sbin/ss…"   About an hour ago   Up About an hour    0.0.0.0:32782->2181/tcp, 0.0.0.0:32781->8030/tcp, 0.0.0.0:32780->8031/tcp, 0.0.0.0:32779->8032/tcp, 0.0.0.0:32778->8033/tcp, 0.0.0.0:32777->8080/tcp, 0.0.0.0:32776->8081/tcp, 0.0.0.0:32775->8088/tcp, 0.0.0.0:32774->8090/tcp, 0.0.0.0:32773->9000/tcp, 0.0.0.0:32772->19888/tcp, 0.0.0.0:32771->49001/tcp, 0.0.0.0:32770->50070/tcp, 0.0.0.0:32769->50075/tcp, 0.0.0.0:32768->50475/tcp   master
```

> 备注：这些端口是总结出来的跟hadoop，zk，spark等有关的一些端口，将之映射到宿主机上的空闲端口上，并且因为服务器上总有些端口是被别的服务应用占用的，所以创建端口映射的时候，采用-p \:\:\[容器内端口]的方式来将容器内端口映射到宿主机上的空闲端口上。

### 分别打开三个shell窗口进入容器的命令

```bash
$ docker exec -it master /bin/bash
$ docker exec -it slave1 /bin/bash
$ docker exec -it slave2 /bin/bash
```

### hadoop初始化

进入到master容器内之后，因为hadoop需要进行初始化，所以我们在名称为master的容器内执行命令：

```bash
[root@master logs]# /usr/local/hadoop/bin/hadoop namenode -format
DEPRECATED: Use of this script to execute hdfs command is deprecated.
Instead use the hdfs command for it.

20/03/16 12:45:25 INFO namenode.NameNode: STARTUP_MSG:
/************************************************************
.......此处略去若干行日志信息.......
20/03/16 12:45:26 INFO namenode.NNStorageRetentionManager: Going to retain 1 images with txid >= 0
20/03/16 12:45:26 INFO util.ExitUtil: Exiting with status 0        // 出现Status 0说明我们已经初始化成功了
20/03/16 12:45:26 INFO namenode.NameNode: SHUTDOWN_MSG:
/************************************************************
SHUTDOWN_MSG: Shutting down NameNode at master/192.168.218.221      // 并且我们可以看到NameNode也是master/192.168.218.221
************************************************************/
```

### 分别到各个容器内启动ZK的组件服务

剩下的事情就是分别到ZK服务，已经在Dockerfile中设置了环境变量，直接执行如下命令即可：

```bash
$ zkServer.sh start // 开启命令
$ zkServer.sh stop // 关闭命令
```

### 在master容器内开启Haodoop组件服务

hadoop的组件比较多，并且由于配置了yarn的日志服务，mapreduce的日志服务等，所以需要执行对应的脚本来启动对应的服务：

```bash
# 启动hdfs
$ sudo /usr/local/hadoop/sbin/start-dfs.sh
# 启动yarn
$ sudo /usr/local/hadoop/sbin/start-yarn.sh
# 启动任务历史，并且已在配置文件中配置了spark用yarn cluster提交时，查看日志
$ sudo /usr/local/hadoop/sbin/mr-jobhistory-daemon.sh start historyserver
```

### 在master容器内开启Spark组件服务

执行如下命令即可：

```bash
$ /usr/local/spark/sbin/start-all.sh
```

### 上述服务启动起来之后的结果

![组件运行效果图](Docker大数据容器集群使用文档/组件运行效果图.png)

### 写在最后

里面的大部分组件端口都做了映射，并且验证过了，如果有其他的端口需要做映射的话，请参考[Docker修改运行中容器的端口映射](Docker修改运行中容器的端口映射.md)。