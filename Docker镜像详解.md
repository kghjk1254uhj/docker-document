<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
**Table of Contents**  *generated with [DocToc](https://github.com/thlorenz/doctoc)*

- [Docker镜像详解](#docker%E9%95%9C%E5%83%8F%E8%AF%A6%E8%A7%A3)
  - [获取镜像](#%E8%8E%B7%E5%8F%96%E9%95%9C%E5%83%8F)
  - [配置Docker加速](#%E9%85%8D%E7%BD%AEdocker%E5%8A%A0%E9%80%9F)
  - [查看镜像信息](#%E6%9F%A5%E7%9C%8B%E9%95%9C%E5%83%8F%E4%BF%A1%E6%81%AF)
    - [使用images命令列出镜像](#%E4%BD%BF%E7%94%A8images%E5%91%BD%E4%BB%A4%E5%88%97%E5%87%BA%E9%95%9C%E5%83%8F)
    - [使用tag命令添加镜像标签](#%E4%BD%BF%E7%94%A8tag%E5%91%BD%E4%BB%A4%E6%B7%BB%E5%8A%A0%E9%95%9C%E5%83%8F%E6%A0%87%E7%AD%BE)
    - [使用history命令查看镜像历史](#%E4%BD%BF%E7%94%A8history%E5%91%BD%E4%BB%A4%E6%9F%A5%E7%9C%8B%E9%95%9C%E5%83%8F%E5%8E%86%E5%8F%B2)
    - [使用history命令查看镜像历史](#%E4%BD%BF%E7%94%A8history%E5%91%BD%E4%BB%A4%E6%9F%A5%E7%9C%8B%E9%95%9C%E5%83%8F%E5%8E%86%E5%8F%B2-1)
    - [搜寻镜像](#%E6%90%9C%E5%AF%BB%E9%95%9C%E5%83%8F)
  - [删除和清理镜像](#%E5%88%A0%E9%99%A4%E5%92%8C%E6%B8%85%E7%90%86%E9%95%9C%E5%83%8F)
    - [使用标签删除镜像](#%E4%BD%BF%E7%94%A8%E6%A0%87%E7%AD%BE%E5%88%A0%E9%99%A4%E9%95%9C%E5%83%8F)
    - [使用镜像ID来删除镜像](#%E4%BD%BF%E7%94%A8%E9%95%9C%E5%83%8Fid%E6%9D%A5%E5%88%A0%E9%99%A4%E9%95%9C%E5%83%8F)
    - [清理镜像](#%E6%B8%85%E7%90%86%E9%95%9C%E5%83%8F)
  - [创建镜像](#%E5%88%9B%E5%BB%BA%E9%95%9C%E5%83%8F)
    - [基于已有容器创建](#%E5%9F%BA%E4%BA%8E%E5%B7%B2%E6%9C%89%E5%AE%B9%E5%99%A8%E5%88%9B%E5%BB%BA)
    - [基于本地模板导入](#%E5%9F%BA%E4%BA%8E%E6%9C%AC%E5%9C%B0%E6%A8%A1%E6%9D%BF%E5%AF%BC%E5%85%A5)
    - [基于Dockerfile进行创建](#%E5%9F%BA%E4%BA%8Edockerfile%E8%BF%9B%E8%A1%8C%E5%88%9B%E5%BB%BA)
  - [存出和载入镜像](#%E5%AD%98%E5%87%BA%E5%92%8C%E8%BD%BD%E5%85%A5%E9%95%9C%E5%83%8F)
    - [存出镜像](#%E5%AD%98%E5%87%BA%E9%95%9C%E5%83%8F)
    - [载入镜像](#%E8%BD%BD%E5%85%A5%E9%95%9C%E5%83%8F)
  - [上传镜像](#%E4%B8%8A%E4%BC%A0%E9%95%9C%E5%83%8F)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

# Docker镜像详解

镜像是Docker三大核心概念中最重要的，自Docker诞生之日起镜像就是相关社区最为热门的关键词。

Docker运行容器前需要本地存在对应的镜像，如果镜像不存在，Docker会尝试先从默认镜像仓库下载（默认使用Docker Hub公共注册服务器中的仓库），用户也可以通过配置，使用自定义的镜像仓库。

## 获取镜像

镜像是运行容器的前提，官方的Docker Hub网站已经提供了数十万个镜像供大家开放下载。

可以使用docker \[image] pull命令直接从Docker Hub镜像源来下载镜像。该命令的格式为：

```bash
docker [image] pull NAME[:TAG]
```

例如，获取一个Ubuntu 18.04系统的基础镜像可以使用如下的命令：

```bash
[root@docker ~]# docker pull ubuntu:18.04
18.04: Pulling from library/ubuntu
423ae2b273f4: Pull complete 
de83a2304fa1: Pull complete 
f9a83bce3af0: Pull complete 
b6b53be908de: Pull complete 
Digest: sha256:04d48df82c938587820d7b6006f5071dbbffceb7ca01d2814f81857c631d44df
Status: Downloaded newer image for ubuntu:18.04
docker.io/library/ubuntu:18.04
```

对于Docker镜像来说，如果不显式指定TAG，则默认会选择latest标签，这会下载仓库中最新版本的镜像。

下面的例子将从Docker Hub的Ubuntu仓库下载一个最新版本的Ubuntu操作系统的镜像：

```bash
$ docker pull ubuntu
Using default tag: latest
latest: Pulling from library/ubuntu
...
Digest: sha256:e27e9d7f7f28d67aa9e2d7540bdc2b33254b452ee8e60f388875e5b7d9b2b696
Status: Downloaded newer image for ubuntu:latest
$ docker pull hub.c.163.com/public/ubuntu:18.04
$ docker run -it ubuntu:18.04 bash
root@65663247040f:/# echo "Hello World"
Hello World
root@65663247040f:/# exit
```

该命令实际上下载的就是ubuntu:latest镜像。

> 注意：一般来说，镜像的latest标签意味着该镜像的内容会跟踪最新版本的变更而变化，内容是不稳定的。因此，从稳定性上考虑，不要在生产环境中忽略镜像的标签信息或使用默认的latest标记的镜像。

下载过程中可以看出，镜像文件一般由若干层（layer）组成，6c953ac5d795这样的串是层的唯一id（实际上完整的id包括256比特，64个十六进制字符组成）。使用docker pull命令下载中会获取并输出镜像的各层信息。当不同的镜像包括相同的层时，本地仅存储了层的一份内容，减小了存储空间。

刚开始学习时可能会想到，在不同的镜像仓库服务器的情况下，可能会出现镜像重名的情况。

严格地讲，镜像的仓库名称中还应该添加仓库地址（即registry，注册服务器）作为前缀，只是默认使用的是官方Docker Hub服务，该前缀可以忽略。

例如，docker pull ubuntu:18.04命令相当于docker pull registry.hub.docker.com/ubuntu:18.04命令，即从默认的注册服务器Docker Hub Registry中的ubuntu仓库来下载标记为18.04的镜像。

如果从非官方的仓库下载，则需要在仓库名称前指定完整的仓库地址。例如从网易蜂巢的镜像源来下载ubuntu:18.04镜像，可以使用如下命令，此时下载的镜像名称为hub.c.163.com/public/ubuntu:18.04：

```bash
$ docker pull hub.c.163.com/public/ubuntu:18.04
```

pull子命令支持的选项主要包括：

- -a, --all-tags=true|false：是否获取仓库中的所有镜像，默认为否；
- --disable-content-trust：取消镜像的内容校验，默认为真。

另外，有时需要使用镜像代理服务来加速Docker镜像获取过程，可以在Docker服务启动配置中增加--registry-mirror=proxy_URL来指定镜像代理服务地址，如[https://registry.docker-cn.com](https://registry.docker-cn.com)。

下载镜像到本地后，即可随时使用该镜像了，例如利用该镜像创建一个容器，在其中运行bash应用，执行打印“Hello World”命令：

```bash
[root@docker ~]# docker container run -it ubuntu:18.04 bash
root@a6a6004fa1f7:/# echo "Hello World"
Hello World
root@a6a6004fa1f7:/# exit
exit
```

## 配置Docker加速

点击进入网址：[https://cr.console.aliyun.com/cn-hangzhou/mirrors](https://cr.console.aliyun.com/cn-hangzhou/mirrors)

此处需要一个阿里云的账号，没有的话可以使用支付宝登录，进入之后：

![配置阿里云镜像加速](Docker镜像详解/配置阿里云镜像加速.png)

按照上面的操作就行了。

> 总结：就是在/etc/docker/daemon.json中添加镜像加速地址即可！

## 查看镜像信息

接下来将介绍docker镜像的ls，tag，inspect子命令。

### 使用images命令列出镜像

使用docker images或docker image ls命令可以列出本地主机上已有镜像的基本信息。

```bash
[root@docker ~]# docker images
REPOSITORY          TAG                 IMAGE ID            CREATED             SIZE
ubuntu              18.04               72300a873c2c        2 weeks ago         64.2MB
ubuntu              latest              72300a873c2c        2 weeks ago         64.2MB
hello-world         latest              fce289e99eb9        14 months ago       1.84kB
```

在列出信息中，可以看到几个字段信息：

- 来自于哪个仓库，比如ubuntu表示ubuntu系列的基础镜像；
- 镜像的标签信息，比如18.04、latest表示不同的版本信息，标签只是标记，并不能标识镜像内容；
- 镜像的ID（唯一标识镜像），如果两个镜像的ID相同，说明它们实际上指向了同一个镜像，只是具有不同标签名称而已；
- 创建时间，说明镜像最后的更新时间；
- 镜像大小，优秀的镜像往往体积都较小。

其中镜像的ID信息十分重要，它唯一标识了镜像。在使用镜像ID的时候，一般可以使用该ID的前若干个字符组成的可区分串来替代完整的ID。

TAG信息用于标记来自同一个仓库的不同镜像。例如ubuntu仓库中有多个镜像，通过TAG信息来区分发行版本，如18.04、18.10等。

镜像大小信息只是表示了该镜像的逻辑体积大小，实际上由于相同的镜像层本地只会存储一份，物理上占用的存储空间会小于各镜像逻辑体积之和。

images子命令主要支持如下选项，用户可以自行进行尝试：

- -a, --all=true|false：列出所有（包括临时文件）镜像文件，默认为否；
- --digests=true|false：列出镜像的数字摘要值，默认为否；
- -f, --filter=[]：过滤列出的镜像，如dangling=true只显示没有被使用的镜像；也可指定带有特定标注的镜像等；
- --format="TEMPLATE"：控制输出格式，如.ID代表ID信息，.Repository代表仓库信息等；
- --no-trunc=true|false：对输出结果中太长的部分是否进行截断，如镜像的ID信息，默认为是；
- -q, --quiet=true|false：仅输出ID信息，默认为否。

其中，还支持对输出结果进行控制的选项，如-f. --filter=[]、--no-trunc=true|false、-q、--quiet=true|false等。

更多子命令选项还可以通过man docker-images来查看。

### 使用tag命令添加镜像标签

为了方便在后续工作中使用特定镜像，还可以使用docker tag命令来为本地镜像任意添加新的标签。例如，添加一个新的myubuntu:latest镜像标签：

```bash
[root@docker ~]# docker tag ubuntu:latest myubuntu:latest
```

再次使用docker images列出本地主机上镜像信息，可以看到多了一个myubuntu:latest标签的镜像：

```bash
[root@docker ~]# docker images
REPOSITORY          TAG                 IMAGE ID            CREATED             SIZE
myubuntu            latest              72300a873c2c        2 weeks ago         64.2MB
ubuntu              18.04               72300a873c2c        2 weeks ago         64.2MB
ubuntu              latest              72300a873c2c        2 weeks ago         64.2MB
hello-world         latest              fce289e99eb9        14 months ago       1.84kB
```

之后，用户就可以直接使用myubuntu:latest来表示这个镜像了。

细心的读者可能注意到，这些myubuntu:latest镜像的ID跟ubuntu:latest是完全一致的，它们实际上指向了同一个镜像文件，只是别名不同而已。docker tag命令添加的标签实际上起到了类似链接的作用。

### 使用history命令查看镜像历史

使用docker inspect \[image]命令可以获取该镜像的详细信息，包括制作者、适应架构、各层的数字摘要等：

```bash
[root@docker ~]# docker inspect ubuntu:18.04
[
    {
        "Id": "sha256:72300a873c2ca11c70d0c8642177ce76ff69ae04d61a5813ef58d40ff66e3e7c",
        "RepoTags": [
            "myubuntu:latest",
            "ubuntu:18.04",
            "ubuntu:latest"
        ],
        "RepoDigests": [
            "ubuntu@sha256:04d48df82c938587820d7b6006f5071dbbffceb7ca01d2814f81857c631d44df"
        ],
        "Parent": "",
        "Comment": "",
        "Created": "2020-02-21T22:20:44.446608273Z",
        "Container": "9aff20b5d709f62597e78d9baff493ab89adea8aa2f7e590673be2231c1d886a",
        "ContainerConfig": {
            "Hostname": "9aff20b5d709",
            "Domainname": "",
            "User": "",
            "AttachStdin": false,
            "AttachStdout": false,
            "AttachStderr": false,
            "Tty": false,
            "OpenStdin": false,
            "StdinOnce": false,
            "Env": [
                "PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin"
            ],
            "Cmd": [
                "/bin/sh",
                "-c",
                "#(nop) ",
                "CMD [\"/bin/bash\"]"
            ],
            "ArgsEscaped": true,
            "Image": "sha256:a3267a9832e16ae5f1e3235d6c65fbd552f42f58cc7f72ec19027d54839e5fe6",
            "Volumes": null,
            "WorkingDir": "",
            "Entrypoint": null,
            "OnBuild": null,
            "Labels": {}
        },
        "DockerVersion": "18.09.7",
        "Author": "",
        "Config": {
            "Hostname": "",
            "Domainname": "",
            "User": "",
            "AttachStdin": false,
            "AttachStdout": false,
            "AttachStderr": false,
            "Tty": false,
            "OpenStdin": false,
            "StdinOnce": false,
            "Env": [
                "PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin"
            ],
            "Cmd": [
                "/bin/bash"
            ],
            "ArgsEscaped": true,
            "Image": "sha256:a3267a9832e16ae5f1e3235d6c65fbd552f42f58cc7f72ec19027d54839e5fe6",
            "Volumes": null,
            "WorkingDir": "",
            "Entrypoint": null,
            "OnBuild": null,
            "Labels": null
        },
        "Architecture": "amd64",
        "Os": "linux",
        "Size": 64206117,
        "VirtualSize": 64206117,
        "GraphDriver": {
            "Data": {
                "LowerDir": "/var/lib/docker/overlay2/4c5700c3041a13cc1317cabf34192277edcbcbc666b2fd342cae15d825e50220/diff:/var/lib/docker/overlay2/c433a5ddc8e601314a43f4a46b5b8c2c76766661b15a53f4d89444257bbc968c/diff:/var/lib/docker/overlay2/2363bfb6494e3f509bfef2a2cefd27eaa4ea8ea64f1ad367645c23a2e4803215/diff",
                "MergedDir": "/var/lib/docker/overlay2/3c254b79a58f0c7241aaf358b82b09522c82f7df3a83ee2a4e1a4c1e7719bb83/merged",
                "UpperDir": "/var/lib/docker/overlay2/3c254b79a58f0c7241aaf358b82b09522c82f7df3a83ee2a4e1a4c1e7719bb83/diff",
                "WorkDir": "/var/lib/docker/overlay2/3c254b79a58f0c7241aaf358b82b09522c82f7df3a83ee2a4e1a4c1e7719bb83/work"
            },
            "Name": "overlay2"
        },
        "RootFS": {
            "Type": "layers",
            "Layers": [
                "sha256:cc4590d6a7187ce8879dd8ea931ffaa18bc52a1c1df702c9d538b2f0c927709d",
                "sha256:8c98131d2d1d1c8339d268265005394de6cafe887020dcca9ba9d9d07a56280c",
                "sha256:03c9b9f537a4ae66d7ae7a4361e7f36e6755380107eadff3fbc11cd604c6c9b9",
                "sha256:1852b2300972ff9f68fd3d34f1f112df3e35757d91dcd40cc8b379bbf2be62d5"
            ]
        },
        "Metadata": {
            "LastTagTime": "2020-03-09T23:16:56.657435819-04:00"
        }
    }
]
```

上面代码返回的是一个JSON格式的消息，如果我们只要其中一项内容时，可以使用-f来指定，例如，获取镜像的Architecture：

```bash
[root@docker ~]# docker inspect -f {{".Architecture"}} ubuntu:18.04
amd64
```

### 使用history命令查看镜像历史

既然镜像文件由多个层组成，那么怎么知道各个层的内容具体是什么呢？这时候可以使用history子命令，该命令将列出各层的创建信息。

例如，查看ubuntu:18.04镜像的创建过程，可以使用如下命令：

```bash
[root@docker ~]# docker history ubuntu:18.04
IMAGE               CREATED             CREATED BY                                      SIZE                COMMENT
72300a873c2c        2 weeks ago         /bin/sh -c #(nop)  CMD ["/bin/bash"]            0B                  
<missing>           2 weeks ago         /bin/sh -c mkdir -p /run/systemd && echo 'do…   7B                  
<missing>           2 weeks ago         /bin/sh -c set -xe   && echo '#!/bin/sh' > /…   745B                
<missing>           2 weeks ago         /bin/sh -c [ -z "$(apt-get indextargets)" ]     987kB               
<missing>           2 weeks ago         /bin/sh -c #(nop) ADD file:91a750fb184711fde…   63.2MB
```

注意，过长的命令被自动截断了，可以使用前面提到的--no-trunc选项来输出完整命令。

### 搜寻镜像

使用docker search命令可以搜索Docker Hub官方仓库中的镜像。语法为docker search \[option] keyword。支持的命令选项主要包括：

- -f, --filter filter：过滤输出内容；
- --format string：格式化输出内容；
- --limit int：限制输出结果个数，默认为25个；
- --no-trunc：不截断输出结果。

例如，搜索官方提供的带nginx关键字的镜像，如下所示：

```bash
[root@docker ~]# docker search --filter=is-official=true nginx
NAME                DESCRIPTION                STARS               OFFICIAL            AUTOMATED
nginx               Official build of Nginx.   12780               [OK]  
```

再比如，搜索所有收藏数超过4的关键词包括tensorflow的镜像：

```bash
[root@docker ~]# docker search --filter=stars=4 tensorflow
NAME                             DESCRIPTION                                     STARS               OFFICIAL            AUTOMATED
tensorflow/tensorflow            Official Docker images for the machine learn…   1634                                    
jupyter/tensorflow-notebook      Jupyter Notebook Scientific Python Stack w/ …   201                                     
tensorflow/serving               Official images for TensorFlow Serving (http…   77                                      
xblaster/tensorflow-jupyter      Dockerized Jupyter with tensorflow              52                                      [OK]
rocm/tensorflow                  Tensorflow with ROCm backend support            40                                      
floydhub/tensorflow              tensorflow                                      24                                      [OK]
bitnami/tensorflow-serving       Bitnami Docker Image for TensorFlow Serving     14                                      [OK]
opensciencegrid/tensorflow-gpu   TensorFlow GPU set up for OSG                   10                                      
ibmcom/tensorflow-ppc64le        Community supported ppc64le docker images fo…   5                                       
tensorflow/tf_grpc_test_server   Testing server for GRPC-based distributed ru…   4  
```

可以看到返回了很多包含关键字的镜像，其中包括镜像名字、描述、收藏数（表示该镜像的受欢迎程度）、是否官方创建、是否自动创建等。默认的输出结果将按照星级评价进行排序。

## 删除和清理镜像

接下来主要介绍Docker镜像的rm和prune子命令。

### 使用标签删除镜像

使用docker rmi或docker image rm命令可以删除镜像，命令格式为docker rmi IMAGE\[IMAGE...]，其中IMAGE可以为标签或ID。

支持选项包括：

- -f, -force：强制删除镜像，即使有容器依赖它；
- -no-prune：不要清理未带标签的父镜像。

例如，要删除掉myubuntu:latest镜像，可以使用如下命令：

```bash
[root@docker ~]# docker rmi myubuntu:latest
Untagged: myubuntu:latest
```

可能会想到，本地的ubuntu:latest镜像是否会受到此命令的影响。无须担心，当同一个镜像拥有多个标签的时候，docker rmi命令只是删除了该镜像多个标签中的指定标签而已，并不影响镜像文件。因此上述操作相当于只是删除了镜像0458a4468cbc的一个标签副本而已。

再次查看本地的镜像，发现ubuntu:latest镜像仍然存在：

```bash
[root@docker ~]# docker images
REPOSITORY          TAG                 IMAGE ID            CREATED             SIZE
ubuntu              18.04               72300a873c2c        2 weeks ago         64.2MB
ubuntu              latest              72300a873c2c        2 weeks ago         64.2MB
hello-world         latest              fce289e99eb9        14 months ago       1.84kB
```

但是当镜像只剩下一个标签的时候就要小心了，此时再使用docker rmi命令会彻底删除镜像。

例如通过执行docker rmi命令来删除只有一个标签的镜像，可以看出会删除这个镜像文件的所有文件层：

```bash
$ docker rmi busybox:latest
Untagged: busybox:latest
Untagged: busybox@sha256:1669a6aa7350e1cdd28f972ddad5aceba2912f589f19a090ac75b7083da748db
Deleted: sha256:5b0d59026729b68570d99bc4f3f7c31a2e4f2a5736435641565d93e7c25bd2c3
Deleted: sha256:4febd3792a1fb2153108b4fa50161c6ee5e3d16aa483a63215f936a113a88e9a
```

### 使用镜像ID来删除镜像

当使用docker rmi命令，并且后面跟上镜像的ID（也可以是能进行区分的部分ID串前缀）时，会先尝试删除所有指向该镜像的标签，然后删除该镜像文件本身。

注意，当有该镜像创建的容器存在时，镜像文件默认是无法被删除的，例如：先利用ubuntu:18.04镜像创建一个简单的容器来输出一段话：

```bash
[root@docker ~]# docker run ubuntu:18.04 echo 'hello! I am here!'
hello! I am here!
```

接下来使用docker ps -a命令可以看到本机上存在的所有容器：

```bash
[root@docker ~]# docker ps -a
CONTAINER ID        IMAGE               COMMAND                  CREATED             STATUS                      PORTS               NAMES
cef4b05ebbee        ubuntu:18.04        "echo 'hello! I am h…"   13 seconds ago      Exited (0) 11 seconds ago                       great_mendel
a6a6004fa1f7        ubuntu:18.04        "bash"                   4 hours ago         Exited (0) 4 hours ago                          vigilant_swartz
a23c9987848e        hello-world         "/hello"                 21 hours ago        Exited (0) 21 hours ago                         stupefied_sutherland
98fa78e7a69c        hello-world         "/hello"                 21 hours ago        Exited (0) 21 hours ago                         gifted_khayyam
```

可以看到，后台存在一个退出状态的容器，是刚基于ubuntu:18.04镜像创建的。

试图删除该镜像，Docker会提示有容器正在运行，无法删除：

```bash
$ docker rmi ubuntu:18.04
Error  response  from  daemon:  conflict:  unable  to  remove  repository  reference "ubuntu:18.04" (must force) - container a21c0840213e is using its referenced image 8f1bd21bd25c
```

如果要想强行删除镜像，可以使用-f参数：

```bash
$ docker rmi -f ubuntu:18.04
Untagged: ubuntu:18.04
Deleted: sha256:8f1bd21bd25c3fb1d4b00b7936a73a0664f932e11406c48a0ef19d82fd0b7342
```

注意，通常并不推荐使用-f参数来强制删除一个存在容器依赖的镜像。正确的做法是，先删除依赖该镜像的所有容器，再来删除镜像。

首先删除容器a21c0840213e：

```bash
$ docker rm a21c0840213e
```

然后使用ID来删除镜像，此时会正常打印出删除的各层信息：

```bash
$ docker rmi 8f1bd21bd25c
Untagged: ubuntu:18.04
Deleted: sha256:8f1bd21bd25c3fb1d4b00b7936a73a0664f932e11406c48a0ef19d82fd0b7342
Deleted: sha256:8ea3b9ba4dd9d448d1ca3ca7afa8989d033532c11050f5e129d267be8de9c1b4
Deleted: sha256:7db5fb90eb6ffb6b5418f76dde5f685601fad200a8f4698432ebf8ba80757576
Deleted: sha256:19a7e879151723856fb640449481c65c55fc9e186405dd74ae6919f88eccce75
Deleted: sha256:c357a3f74f16f61c2cc78dbb0ae1ff8c8f4fa79be9388db38a87c7d8010b2fe4
Deleted: sha256:a7e1c363defb1f80633f3688e945754fc4c8f1543f07114befb5e0175d569f4c
```

### 清理镜像

使用Docker一段时间后，系统中可能会遗留一些临时的镜像文件，以及一些没有被使用的镜像，可以通过docker image prune命令来进行清理。

支持的选项包括：

- -a, -all：删除所有无用镜像，不光是临时镜像；
- -filter filter：只清理符合给定过滤器的镜像；
- -f, -force：强制删除镜像，而不进行提示确认。

例如，如下命令会自动清理临时的遗留镜像文件层，最后会提示释放的存储空间：

```bash
$ docker image prune -f
...
Total reclaimed space: 1.4 GB
```

## 创建镜像

创建镜像的方法主要有三种：基于已有镜像的容器创建、基于本地模板导入、基于Dockerfile创建。

这里仅仅先介绍前面两种，后面的后续介绍。

### 基于已有容器创建

该方法主要是使用docker \[container] commit命令。

命令格式为：

```bash
docker [container] commit [OPTIONS] CONTAINER [REPOSITORY [:TAG]]
```

主要选项包括：

- -a, --author=""：作者信息；
- -c, --change=\[]：提交的时候执行Dockerfile指令，包括CMD|ENTRYPOINT|ENV|EXPOSE|LABEL|ONBUILD|USER|VOLUME|WORKDIR等；
- -m, --message=""：提交消息；
- -p, --pause=true：提交时暂停容器运行。

其中container参数可选。

下面将演示如何使用该命令创建一个新镜像：

首先，启动一个镜像，并在其中进行修改操作。例如，创建一个test文件，之后退出，代码如下：

```bash
[root@docker ~]# docker run -it ubuntu:18.04 /bin/bash
root@58086eb0f388:/# touch test
root@58086eb0f388:/# exit
exit
```

此时容器的ID为：58086eb0f388。

此时该容器与原ubuntu:18.04镜像相比，已经发生了改变，可以使用docker \[container]commit命令来提交为一个新的镜像。提交时可以使用ID或名称来指定容器：

```bash
[root@docker ~]# docker container commit -m "Added a new file" -a "hchihping" 58086eb0f388 test:0.1
sha256:c1c8dc917b59d1830f558887b62ee6e82ba36f358882d4047c46bf3352d16a7c
```

顺利的话，会返回新创建镜像的ID信息，例如sha256:c1c8dc917b59d1830f558887b62ee6e82ba36f358882d4047c46bf3352d16a7c。

此时查看本地镜像列表，会发现新创建的镜像已经存在了：

```bash
[root@docker ~]# docker images
REPOSITORY          TAG                 IMAGE ID            CREATED              SIZE
test                0.1                 c1c8dc917b59        About a minute ago   64.2MB
```

### 基于本地模板导入

用户也可以直接从一个操作系统模板文件导入一个镜像，主要使用docker \[container] import命令。

命令格式为：

```bash
docker [image] import [OPTIONS] file|URL|-[REPOSITORY [:TAG]]
```

例如，下载了ubuntu-14.04-x86_64-minimal.tar.gz的模板压缩包，之后使用以下命令导入即可：

```bash
[root@docker opt]# cat ubuntu-14.04-x86_64-minimal.tar.gz | docker import - ubuntu:14.04
sha256:c3bde84b5a42da6549c345143f59f7c6b6c9a08c38cf928e5aeaf87374a50b23
```

然后查看新导入的镜像，已经在本地存在了：

```bash
[root@docker opt]# docker images
REPOSITORY          TAG                 IMAGE ID            CREATED              SIZE
ubuntu              14.04               c3bde84b5a42        About a minute ago   215MB
```

### 基于Dockerfile进行创建

基于Dockerfile创建是最常见的方式。Dockerfile是一个文本文件，利用给定的指令描述基于某个父镜像创建新镜像的过程。

下面给出Dockerfile的一个简单示例，基于debian:jessie镜像安装redis:3.2.5环境，构成一个新的redis:3.2.5镜像：

1.首先创建一个Dockerfile，文件内容如下：

```bash
FROM debian:jessie

LABEL version="1.0" maintainer="hchihping"

RUN buildDeps='gcc libc6-dev make wget' \
    && apt-get update \
    && apt-get install -y $buildDeps \
    && wget -O redis.tar.gz "http://download.redis.io/releases/redis-3.2.5.tar.gz" \
    && mkdir -p /usr/src/redis \
    && tar -xzf redis.tar.gz -C /usr/src/redis --strip-components=1 \
    && make -C /usr/src/redis \
    && make -C /usr/src/redis install \
    && rm -rf /var/lib/apt/lists/* \
    && rm redis.tar.gz \
    && rm -r /usr/src/redis \
    && apt-get purge -y --auto-remove $buildDeps
```

2.执行构建命令

```bash
// 进入Dockerfile文件所在目录之下，然后执行如下命令：
$ docker image build -t redis:3.2.5 .
```

3.查看构建结果

```bash
[root@docker docker]# docker images
REPOSITORY                  TAG                 IMAGE ID            CREATED             SIZE
redis                       3.2.5               f485c96c44d3        About an hour ago   147MB
```

## 存出和载入镜像

介绍Docker镜像的save和load子命令。用户可以使用docker \[image] save和docker \[image] load命令来存出和载入镜像。

### 存出镜像

如果要导出镜像到本地文件，可以使用docker \[image] save命令。该命令支持-o、-outputstring参数，导出镜像到指定的文件中。

> \[image]表示image参数是可选参数。

例如，导出本地的ubuntu:18.04镜像为文件ubuntu_18.04.tar.gz，如下所示：

```bash
[root@docker ~]# docker image save -o ubuntu_18.04.tar.gz ubuntu:18.04
```

之后，用户就可以通过复制ubuntu_18.04.tar.gz文件将该镜像分享给他人。

### 载入镜像

可以使用docker \[image] load将导出的tar文件再导入到本地镜像库。支持-i、-input string选项，从指定文件中读入镜像内容。

例如，从文件ubuntu_18.04.tar.gz导入镜像到本地镜像列表，如下所示：

```bash
$ docker load -i ubuntu_18.04.tar.gz
```

或者：

```bash
$ docker load < ubuntu_18.04.tar.gz
```

这将导入镜像及其相关的元数据信息（包括标签等）。导入成功后，可以使用docker images命令进行查看，与原镜像一致。

## 上传镜像

下面主要介绍Docker镜像的push子命令。可以使用docker \[image] push命令上传镜像到仓库，默认上传到Docker Hub官方仓库（需要登录）。

命令格式为：

```bash
docker [image] push NAME[:TAG] | [REGISTRY_HOST[:REGISTRY_PORT]/]NAME[:TAG]
```

用户在Docker Hub网站注册后可以上传自制的镜像。例如，用户hchihping上传本地的docker-learning:18.04镜像，可以先添加新的标签hchihping/docker-learning:18.04，然后用docker \[image] push命令上传镜像：

```bash
// 对本地的镜像打一个标签，hchihping表示你在docker注册的用户名，docker-learning表示你即将push的仓库名称，18.04表示标签名称。
[root@docker ~]# docker tag ubuntu:18.04 hchihping/docker-learning:18.04

[root@docker ~]# docker login
Login with your Docker ID to push and pull images from Docker Hub. If you don't have a Docker ID, head over to https://hub.docker.com to create one.
Username: hchihping
Password: 
WARNING! Your password will be stored unencrypted in /root/.docker/config.json.
Configure a credential helper to remove this warning. See
https://docs.docker.com/engine/reference/commandline/login/#credentials-store
Login Succeeded

[root@docker ~]# docker push hchihping/docker-learning:18.04
The push refers to repository [docker.io/hchihping/docker-learning]
1852b2300972: Mounted from library/ubuntu 
03c9b9f537a4: Mounted from library/ubuntu 
8c98131d2d1d: Mounted from library/ubuntu 
cc4590d6a718: Mounted from library/ubuntu 
18.04: digest: sha256:0925d086715714114c1988f7c947db94064fd385e171a63c07730f1fa014e6f9 size: 1152
```

> 注意：执行push之前，需要先执行docker login登录到自己的用户仓库。