<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
**Table of Contents**  *generated with [DocToc](https://github.com/thlorenz/doctoc)*

- [Docker搭建大数据组件集群的全过程](#docker%E6%90%AD%E5%BB%BA%E5%A4%A7%E6%95%B0%E6%8D%AE%E7%BB%84%E4%BB%B6%E9%9B%86%E7%BE%A4%E7%9A%84%E5%85%A8%E8%BF%87%E7%A8%8B)
  - [自定义网络](#%E8%87%AA%E5%AE%9A%E4%B9%89%E7%BD%91%E7%BB%9C)
  - [构建基础镜像](#%E6%9E%84%E5%BB%BA%E5%9F%BA%E7%A1%80%E9%95%9C%E5%83%8F)
  - [在centos:v1.0的基础上创建三个容器](#%E5%9C%A8centosv10%E7%9A%84%E5%9F%BA%E7%A1%80%E4%B8%8A%E5%88%9B%E5%BB%BA%E4%B8%89%E4%B8%AA%E5%AE%B9%E5%99%A8)
  - [修改ssh配置文件](#%E4%BF%AE%E6%94%B9ssh%E9%85%8D%E7%BD%AE%E6%96%87%E4%BB%B6)
    - [配置master](#%E9%85%8D%E7%BD%AEmaster)
    - [配置slave1](#%E9%85%8D%E7%BD%AEslave1)
    - [配置slave2](#%E9%85%8D%E7%BD%AEslave2)
  - [生成authorized_keys](#%E7%94%9F%E6%88%90authorized_keys)
  - [验证ssh互信](#%E9%AA%8C%E8%AF%81ssh%E4%BA%92%E4%BF%A1)
    - [验证master](#%E9%AA%8C%E8%AF%81master)
    - [验证slave1](#%E9%AA%8C%E8%AF%81slave1)
    - [验证slave2](#%E9%AA%8C%E8%AF%81slave2)
  - [设置完之后，停止并提交](#%E8%AE%BE%E7%BD%AE%E5%AE%8C%E4%B9%8B%E5%90%8E%E5%81%9C%E6%AD%A2%E5%B9%B6%E6%8F%90%E4%BA%A4)
  - [关闭容器后，重启问题](#%E5%85%B3%E9%97%AD%E5%AE%B9%E5%99%A8%E5%90%8E%E9%87%8D%E5%90%AF%E9%97%AE%E9%A2%98)
  - [使用容器提交的镜像，启动新的容器验证ssh](#%E4%BD%BF%E7%94%A8%E5%AE%B9%E5%99%A8%E6%8F%90%E4%BA%A4%E7%9A%84%E9%95%9C%E5%83%8F%E5%90%AF%E5%8A%A8%E6%96%B0%E7%9A%84%E5%AE%B9%E5%99%A8%E9%AA%8C%E8%AF%81ssh)
  - [思路设计](#%E6%80%9D%E8%B7%AF%E8%AE%BE%E8%AE%A1)
    - [Dockerfile文件制作](#dockerfile%E6%96%87%E4%BB%B6%E5%88%B6%E4%BD%9C)
  - [构建结果测试](#%E6%9E%84%E5%BB%BA%E7%BB%93%E6%9E%9C%E6%B5%8B%E8%AF%95)
    - [scala、java、zookeeper对应测试](#scalajavazookeeper%E5%AF%B9%E5%BA%94%E6%B5%8B%E8%AF%95)
    - [hadoop配置文件修改拷贝到宿主机](#hadoop%E9%85%8D%E7%BD%AE%E6%96%87%E4%BB%B6%E4%BF%AE%E6%94%B9%E6%8B%B7%E8%B4%9D%E5%88%B0%E5%AE%BF%E4%B8%BB%E6%9C%BA)
      - [修改hadoop-env.sh](#%E4%BF%AE%E6%94%B9hadoop-envsh)
      - [修改slaves文件](#%E4%BF%AE%E6%94%B9slaves%E6%96%87%E4%BB%B6)
      - [修改core-site.xml文件](#%E4%BF%AE%E6%94%B9core-sitexml%E6%96%87%E4%BB%B6)
      - [修改hdfs-site.xml](#%E4%BF%AE%E6%94%B9hdfs-sitexml)
      - [修改mapred-site.xml文件](#%E4%BF%AE%E6%94%B9mapred-sitexml%E6%96%87%E4%BB%B6)
      - [修改yarn-site.xml](#%E4%BF%AE%E6%94%B9yarn-sitexml)
    - [创建文件夹](#%E5%88%9B%E5%BB%BA%E6%96%87%E4%BB%B6%E5%A4%B9)
    - [执行初始化](#%E6%89%A7%E8%A1%8C%E5%88%9D%E5%A7%8B%E5%8C%96)
    - [spark配置文件修改拷贝到宿主机](#spark%E9%85%8D%E7%BD%AE%E6%96%87%E4%BB%B6%E4%BF%AE%E6%94%B9%E6%8B%B7%E8%B4%9D%E5%88%B0%E5%AE%BF%E4%B8%BB%E6%9C%BA)
    - [启动容器并设置一些端口映射](#%E5%90%AF%E5%8A%A8%E5%AE%B9%E5%99%A8%E5%B9%B6%E8%AE%BE%E7%BD%AE%E4%B8%80%E4%BA%9B%E7%AB%AF%E5%8F%A3%E6%98%A0%E5%B0%84)
    - [集群组件常用的一些端口解释（为完善完全）](#%E9%9B%86%E7%BE%A4%E7%BB%84%E4%BB%B6%E5%B8%B8%E7%94%A8%E7%9A%84%E4%B8%80%E4%BA%9B%E7%AB%AF%E5%8F%A3%E8%A7%A3%E9%87%8A%E4%B8%BA%E5%AE%8C%E5%96%84%E5%AE%8C%E5%85%A8)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

# Docker搭建大数据组件集群的全过程

## 自定义网络

```bash
$ docker network create --subnet=192.168.218.221/24 mynetwork
$ docker network ls
NETWORK ID          NAME                DRIVER              SCOPE
20d4dd3ee621        bridge              bridge              local
4f31fb4c4d02        host                host                local
006c3401568f        mynetwork           bridge              local
55e4a7003b8e        none                null                local
```

## 构建基础镜像

```bash
$ docker run -it centos:centos7.6.1810 // 物理机上运行从docker hub上pull下来的基础镜像
[root@e4aef6383f9e /] # yum install -y net-tools openssh-clients vim which // 在容器中安装相关需要的软件，为ssh做准备
[root@e4aef6383f9e /] # exit // 退出容器
$ docker ps -a // 查看推出的容器的名称
CONTAINER ID        IMAGE                   COMMAND                  CREATED             STATUS                     PORTS               NAMES
e4aef6383f9e        centos:centos7.6.1810   "/bin/bash"              8 minutes ago       Exited (0) 6 seconds ago                       bold_mirzakhani
$ docker container commit bold_mirzakhani centos:v1.0 // 提交推出的容器为新的镜像，命名为centos:v1.0
sha256:a7ecb036e1fbd3e7946b3bd5b9a3f238cfb69c6d46e583278e937cbae54891ef
```

## 在centos:v1.0的基础上创建三个容器

使用centos:v1.0镜像创建三个容器，分别指定镜像名称，hostname，添加/etc/hosts中的数据，指定自定义网络以及IP

容器名称 | 容器的hostname | 容器的IP
---------|----------|---------
 master | master | 192.168.218.221
 slave1 | slave1 | 192.168.218.222
 slave2 | slave2 | 192.168.218.223

```bash
$ docker run -itd --name master -h master --add-host master:192.168.218.221 --add-host slave1:192.168.218.222 --add-host slave2:192.168.218.223 --net=mynetwork --ip=192.168.218.221 centos:v1.0

$ docker run -itd --name slave1 -h slave1 --add-host master:192.168.218.221 --add-host slave1:192.168.218.222 --add-host slave2:192.168.218.223 --net=mynetwork --ip=192.168.218.222 centos:v1.0

$ docker run -itd --name slave2 -h slave2 --add-host master:192.168.218.221 --add-host slave1:192.168.218.222 --add-host slave2:192.168.218.223 --net=mynetwork --ip=192.168.218.223 centos:v1.0
```

> 凡大数据集群，请使用上述的几种方式启动对应的

![创建三个容器](Docker搭建大数据组件集群的全过程/创建三个容器.png)

## 修改ssh配置文件

### 配置master

```bash
$ docker exec -it master /bin/bash // 进入master容器
[root@master /]# yum install -y passwd openssl openssh-server
[root@master /]# /usr/sbin/sshd // 报错文件找不到
Could not load host key: /etc/ssh/ssh_host_rsa_key
Could not load host key: /etc/ssh/ssh_host_ecdsa_key
Could not load host key: /etc/ssh/ssh_host_ed25519_key
sshd: no hostkeys available -- exiting.
[root@master /]# ssh-keygen -q -t rsa -b 2048 -f /etc/ssh/ssh_host_rsa_key -N ''
[root@master /]# ssh-keygen -q -t ecdsa -f /etc/ssh/ssh_host_ecdsa_key -N ''
[root@master /]# ssh-keygen -t dsa -f /etc/ssh/ssh_host_ed25519_key -N ''
Generating public/private dsa key pair.
Your identification has been saved in /etc/ssh/ssh_host_ed25519_key.
Your public key has been saved in /etc/ssh/ssh_host_ed25519_key.pub.
The key fingerprint is:
SHA256:gt+01O3hp6joDedCw/z7wcKMb0AhWEiRuavhZS2UH9Y root@master
The key's randomart image is:
+---[DSA 1024]----+
| .oB.            |
|  = . .          |
|   .....         |
|  .o +.E . .     |
|  ..=+o S . o    |
|. .+ +*O o o .   |
|.oo ..++B o o .  |
|..    .Bo. o o   |
|     .oo*+o .    |
+----[SHA256]-----+
// UsePAM yes 改为 UsePAM no 
// UsePrivilegeSeparation sandbox 改为 UsePrivilegeSeparation no
// 具体执行如下：
[root@master /]# sed -i "s/#UsePrivilegeSeparation.*/UsePrivilegeSeparation no/g" /etc/ssh/sshd_config
[root@master /]# sed -i "s/UsePAM.*/UsePAM no/g" /etc/ssh/sshd_config
[root@master /]# /usr/sbin/sshd
WARNING: 'UsePAM no' is not supported in Red Hat Enterprise Linux and may cause several problems.
[root@master /]# ps -ef | grep ssh
root         62      0  0 02:51 ?        00:00:00 /usr/sbin/sshd
root         67     16  0 02:54 pts/1    00:00:00 grep --color=auto ssh
[root@master /]# passwd
Changing password for user root.
New password:（root）
BAD PASSWORD: The password is shorter than 8 characters
Retype new password:（root）
passwd: all authentication tokens updated successfully.
[root@master /]# ssh-keygen -t rsa
Generating public/private rsa key pair.
Enter file in which to save the key (/root/.ssh/id_rsa):
Created directory '/root/.ssh'.
Enter passphrase (empty for no passphrase):
Enter same passphrase again:
Your identification has been saved in /root/.ssh/id_rsa.
Your public key has been saved in /root/.ssh/id_rsa.pub.
The key fingerprint is:
SHA256:TCdQoKRf7e4b/PGZgqJ9tTir4/mPlgjXwGkIA87oiZI root@master
The key's randomart image is:
+---[RSA 2048]----+
|..  . oo.        |
|+ oo . o         |
|.o.o.o..+ .      |
|o.....=+ o       |
|Eo  .. oS        |
|.   . .o. .      |
|     o .+=..     |
|     .+oB+oo o   |
|    .o=B*=o.+    |
+----[SHA256]-----+
[root@master /]# cd ~/.ssh/
[root@master .ssh]# ll
total 8
-rw-------. 1 root root 1675 Mar 16 02:57 id_rsa
-rw-r--r--. 1 root root  393 Mar 16 02:57 id_rsa.pub
[root@master .ssh]# ps -ef | grep ssh
root         62      0  0 02:51 ?        00:00:00 /usr/sbin/sshd
root         72     16  0 02:58 pts/1    00:00:00 grep --color=auto ssh
```

### 配置slave1

```bash
[root@docker ~]# docker exec -it slave1 /bin/bash
[root@slave1 /]#
[root@slave1 /]# yum install -y passwd openssl openssh-server
[root@slave1 /]# /usr/sbin/sshd
Could not load host key: /etc/ssh/ssh_host_rsa_key
Could not load host key: /etc/ssh/ssh_host_ecdsa_key
Could not load host key: /etc/ssh/ssh_host_ed25519_key
sshd: no hostkeys available -- exiting.
[root@slave1 /]# ssh-keygen -q -t rsa -b 2048 -f /etc/ssh/ssh_host_rsa_key -N ''
[root@slave1 /]# ssh-keygen -q -t ecdsa -f /etc/ssh/ssh_host_ecdsa_key -N ''
[root@slave1 /]# ssh-keygen -t dsa -f /etc/ssh/ssh_host_ed25519_key -N ''
Generating public/private dsa key pair.
Your identification has been saved in /etc/ssh/ssh_host_ed25519_key.
Your public key has been saved in /etc/ssh/ssh_host_ed25519_key.pub.
The key fingerprint is:
SHA256:shIG04M6YWMyDi4izHRL0UeNdRK7EEOsD5ywfLMhz7c root@slave1
The key's randomart image is:
+---[DSA 1024]----+
|   .. +=++..     |
|   +.. +o.+      |
|===o* +. .       |
|@++*.X  . .      |
|*= .B B S.       |
|+. . = =         |
|    . o .        |
|     . E         |
|                 |
+----[SHA256]-----+
[root@slave1 /]# sed -i "s/#UsePrivilegeSeparation.*/UsePrivilegeSeparation no/g" /etc/ssh/sshd_config
[root@slave1 /]# sed -i "s/UsePAM.*/UsePAM no/g" /etc/ssh/sshd_config
[root@slave1 /]# /usr/sbin/sshd
WARNING: 'UsePAM no' is not supported in Red Hat Enterprise Linux and may cause several problems.
[root@slave1 /]# ps -ef | grep ssh
root         75      0  0 03:05 ?        00:00:00 /usr/sbin/sshd
root         77     16  0 03:05 pts/1    00:00:00 grep --color=auto ssh
[root@slave1 /]# passwd
Changing password for user root.
New password:
BAD PASSWORD: The password is shorter than 8 characters
Retype new password:
passwd: all authentication tokens updated successfully.
[root@slave1 /]# ssh-keygen -t rsa
Generating public/private rsa key pair.
Enter file in which to save the key (/root/.ssh/id_rsa):
Created directory '/root/.ssh'.
Enter passphrase (empty for no passphrase):
Enter same passphrase again:
Your identification has been saved in /root/.ssh/id_rsa.
Your public key has been saved in /root/.ssh/id_rsa.pub.
The key fingerprint is:
SHA256:eMIG+t37VE+S70xUQqLowNAhMxY8y/LU9GKX1bKtcWg root@slave1
The key's randomart image is:
+---[RSA 2048]----+
|   .Bo..   .. .  |
|   .o*o  .o..o   |
|   ..=o..o.=  . .|
|  ..+o+++ E o. o |
|  .+ .=oS. ++ o  |
|   ..o +  .. *   |
|    . . . .   +  |
|         o   +   |
|        ...   o  |
+----[SHA256]-----+
[root@slave1 /]# cd ~/.ssh/
[root@slave1 .ssh]# ll
total 8
-rw-------. 1 root root 1675 Mar 16 03:06 id_rsa
-rw-r--r--. 1 root root  393 Mar 16 03:06 id_rsa.pub
[root@slave1 .ssh]# scp id_rsa.pub root@master:~/.ssh/id_rsa.pub.slave1
```

### 配置slave2

```bash
[root@docker ~]# docker exec -it slave2 /bin/bash
[root@slave2 /]#
[root@slave2 /]# yum install -y passwd openssl openssh-server
[root@slave2 /]# /usr/sbin/sshd
Could not load host key: /etc/ssh/ssh_host_rsa_key
Could not load host key: /etc/ssh/ssh_host_ecdsa_key
Could not load host key: /etc/ssh/ssh_host_ed25519_key
sshd: no hostkeys available -- exiting.
[root@slave2 /]# ssh-keygen -q -t rsa -b 2048 -f /etc/ssh/ssh_host_rsa_key -N ''
[root@slave2 /]# ssh-keygen -q -t ecdsa -f /etc/ssh/ssh_host_ecdsa_key -N ''
[root@slave2 /]# ssh-keygen -t dsa -f /etc/ssh/ssh_host_ed25519_key -N ''
Generating public/private dsa key pair.
Your identification has been saved in /etc/ssh/ssh_host_ed25519_key.
Your public key has been saved in /etc/ssh/ssh_host_ed25519_key.pub.
The key fingerprint is:
SHA256:HPxU2IhePlBzmT8DaJPI0bMcS0oyCtX/O60uRe6ZBu0 root@slave2
The key's randomart image is:
+---[DSA 1024]----+
|   ... ..*o*oo   |
|  .   +.* &+=    |
|   . . *oO.* o   |
|    .  .++B   +  |
|        S=..   o |
|        . =      |
|         = =     |
|        . E .    |
|         +oo     |
+----[SHA256]-----+
[root@slave2 /]# sed -i "s/#UsePrivilegeSeparation.*/UsePrivilegeSeparation no/g" /etc/ssh/sshd_config
[root@slave2 /]# sed -i "s/UsePAM.*/UsePAM no/g" /etc/ssh/sshd_config
[root@slave2 /]# /usr/sbin/sshd
WARNING: 'UsePAM no' is not supported in Red Hat Enterprise Linux and may cause several problems.
[root@slave2 /]# ps -ef | grep ssh
root         61      0  0 03:21 ?        00:00:00 /usr/sbin/sshd
root         63     16  0 03:23 pts/1    00:00:00 grep --color=auto ssh
[root@slave2 /]# passwd
Changing password for user root.
New password:
BAD PASSWORD: The password is shorter than 8 characters
Retype new password:
passwd: all authentication tokens updated successfully.
[root@slave2 /]# ssh-keygen -t rsa
Generating public/private rsa key pair.
Enter file in which to save the key (/root/.ssh/id_rsa):
Created directory '/root/.ssh'.
Enter passphrase (empty for no passphrase):
Enter same passphrase again:
Your identification has been saved in /root/.ssh/id_rsa.
Your public key has been saved in /root/.ssh/id_rsa.pub.
The key fingerprint is:
SHA256:jYPx7WHjQXm6qRQ1g5fegLJxEBk9kgJiduHZmYFmt4k root@slave2
The key's randomart image is:
+---[RSA 2048]----+
|.o.+o.+*         |
|o.o++.B.oo o     |
|  ooo=*.+.X .    |
|   E o O O B     |
|      o S X .    |
|         * *     |
|        . =      |
|       . .       |
|        .        |
+----[SHA256]-----+
[root@slave2 /]# cd ~/.ssh/
[root@slave2 .ssh]# ll
total 8
-rw-------. 1 root root 1679 Mar 16 03:23 id_rsa
-rw-r--r--. 1 root root  393 Mar 16 03:23 id_rsa.pub
[root@slave2 .ssh]# scp id_rsa.pub root@master:~/.ssh/id_rsa.pub.slave2
The authenticity of host 'master (192.168.218.221)' can't be established.
ECDSA key fingerprint is SHA256:lIurgszynuToPpumVK22r1IxSsEjsdLb0XJixjj1Ko8.
ECDSA key fingerprint is MD5:f8:63:ef:11:05:2f:29:ea:36:58:6b:2c:20:86:69:1c.
Are you sure you want to continue connecting (yes/no)? yes
Warning: Permanently added 'master,192.168.218.221' (ECDSA) to the list of known hosts.
root@master's password:
id_rsa.pub                                                                                                                                                  100%  393   338.5KB/s   00:00
```

## 生成authorized_keys

进入master容器中的~/.ssh，合并id_rsa_pub.\*到authorized_keys中，然后再发送到slave1容器，slave2容器的~/.ssh。

```bash
[root@master .ssh]# cd ~/.ssh
[root@master .ssh]# ll
total 16
-rw-------. 1 root root 1675 Mar 16 02:57 id_rsa
-rw-r--r--. 1 root root  393 Mar 16 02:57 id_rsa.pub
-rw-r--r--. 1 root root  393 Mar 16 03:09 id_rsa.pub.slave1
-rw-r--r--. 1 root root  393 Mar 16 03:24 id_rsa.pub.slave2
[root@master .ssh]# cat id_rsa.pub >> authorized_keys
[root@master .ssh]# cat id_rsa.pub.slave1 >> authorized_keys
[root@master .ssh]# cat id_rsa.pub.slave2 >> authorized_keys
[root@master .ssh]# ll
total 20
-rw-r--r--. 1 root root 1179 Mar 16 03:29 authorized_keys
-rw-------. 1 root root 1675 Mar 16 02:57 id_rsa
-rw-r--r--. 1 root root  393 Mar 16 02:57 id_rsa.pub
-rw-r--r--. 1 root root  393 Mar 16 03:09 id_rsa.pub.slave1
-rw-r--r--. 1 root root  393 Mar 16 03:24 id_rsa.pub.slave2
[root@master .ssh]# cat authorized_keys
ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQCykQr0fgbbMrZlhNxzHCOcRC3iv6OZNXKTQ4wWNmilK0XgcI1P40657cA2tX+WmzcL6P9QoXF2mEtg5Gd/3I1gbQQGmIi2YRCpNO+uGlx0TxuFLJ7spboU3efFrEh1W9BhfqxzIEMqRw8wPpgdlZXw83R+ZTct4483HZxR2wVHKVvA5DtKP8d8M2tWZkiArenBQf1/ZUt3pM+Bznh6QsujxngBYlis+wsymRV2NUVoisl8KHhTqdhgy+QiMq7g6Q1c163f+fo1y3SdTJuFyvraaEYLHGgyPKp8HF8EO38OiuREciX3tYePKUwEx4q/JTLdBejEvuJQdPDmv/8Pe+sp root@master
ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQDE7DQypovwzEeQPtIv7aO289SInpIoYb+DMbOMv7jnPUHldd6peaYCMs0dRE+mwl3fz1sG4RWxlOa5r71dh5y1RtSjA1Z5JU2SHIqRoUYS+U0U2sZtJeyJnu6C0wVWKdAy5/DtFOXGKZZWcZNZxZPK3bTcL9o33hOlFGHE2r1eFggbMD4a5UtY8jzZYY+EEJYIEHrndTEEerxjk5BruY3xw61pQB4ymnux0EVIkD+Mn6wGK/P/gmyHUqQNgrU3URz8SbIh51ukSOD1yqvMb+VQbrh5buX4b5Bay5ecWIhQt9B/vUeoX+U21sBFqqtQEkwdBBNFUPFurH4IfP0z24Cf root@slave1
ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQC927NwFyEek2VL0eHaCSZY4xoP0wRYofJqAO8PeT7XCt2jMaIjuKGJvJWEMpLh0q1+ZbRMSqaV9Y7vuKxFrDqaFljW/MpRM9dDoN9dmYor4lua1yIZKG2qq4S3vkPPilF66OZpb5QcGQ+dgsFbEYeRmt1PCfybDpp8d78dh/qwUP4pSzToe1aL4RnfzgaRJiPqkWwKVnCpZJZxfD7mc3UnmBdsO84RbRuuWaY23IrK0AaRc4tuhwiXghL58dWOagND2OhcTFqEHJHhkvmm70oBgqDh/muMuHRo34CWhAAxv6szxQ1PqJIgfuhh5kVaHPLp+rK/M589/B6XwwJjJxqv root@slave2
[root@master .ssh]# scp authorized_keys root@slave1:~/.ssh/
The authenticity of host 'slave1 (192.168.218.222)' can't be established.
ECDSA key fingerprint is SHA256:7ZoGa2CvtrbbQKlBTyKoRuGEeCFFsM18OxSN5xh0fG4.
ECDSA key fingerprint is MD5:6a:99:5a:59:c6:8a:af:57:d7:a5:65:ca:8b:bd:a1:1b.
Are you sure you want to continue connecting (yes/no)? yes
Warning: Permanently added 'slave1,192.168.218.222' (ECDSA) to the list of known hosts.
root@slave1's password:
authorized_keys                                                                                                                                             100% 1179     1.3MB/s   00:00
[root@master .ssh]# scp authorized_keys root@slave2:~/.ssh/
The authenticity of host 'slave2 (192.168.218.223)' can't be established.
ECDSA key fingerprint is SHA256:suTPXLIGxNCh2JEseJ7xhYe/7bEUxbsgsavdR3/u4C4.
ECDSA key fingerprint is MD5:b1:89:31:ab:c5:ea:30:5c:69:05:11:21:33:33:a5:f9.
Are you sure you want to continue connecting (yes/no)? yes
Warning: Permanently added 'slave2,192.168.218.223' (ECDSA) to the list of known hosts.
root@slave2's password:
authorized_keys                                                                                                                                             100% 1179   991.4KB/s   00:00
```

## 验证ssh互信

### 验证master

```bash
[root@master .ssh]# ssh master
The authenticity of host 'master (192.168.218.221)' can't be established.
ECDSA key fingerprint is SHA256:lIurgszynuToPpumVK22r1IxSsEjsdLb0XJixjj1Ko8.
ECDSA key fingerprint is MD5:f8:63:ef:11:05:2f:29:ea:36:58:6b:2c:20:86:69:1c.
Are you sure you want to continue connecting (yes/no)? yes
Warning: Permanently added 'master,192.168.218.221' (ECDSA) to the list of known hosts.
[root@master ~]# logout
Connection to master closed.
[root@master .ssh]# ssh slave1
[root@slave1 ~]# logout
Connection to slave1 closed.
[root@master .ssh]# ssh slave2
[root@slave2 ~]# logout
Connection to slave2 closed.
```

### 验证slave1

```bash
[root@slave1 .ssh]# ssh master
Last login: Mon Mar 16 03:33:34 2020 from master
[root@master ~]# logout
Connection to master closed.
[root@slave1 .ssh]# ssh slave1
The authenticity of host 'slave1 (192.168.218.222)' can't be established.
ECDSA key fingerprint is SHA256:7ZoGa2CvtrbbQKlBTyKoRuGEeCFFsM18OxSN5xh0fG4.
ECDSA key fingerprint is MD5:6a:99:5a:59:c6:8a:af:57:d7:a5:65:ca:8b:bd:a1:1b.
Are you sure you want to continue connecting (yes/no)? yes
Warning: Permanently added 'slave1,192.168.218.222' (ECDSA) to the list of known hosts.
Last login: Mon Mar 16 03:32:52 2020 from master
[root@slave1 ~]# logout
Connection to slave1 closed.
[root@slave1 .ssh]# ssh slave2
The authenticity of host 'slave2 (192.168.218.223)' can't be established.
ECDSA key fingerprint is SHA256:suTPXLIGxNCh2JEseJ7xhYe/7bEUxbsgsavdR3/u4C4.
ECDSA key fingerprint is MD5:b1:89:31:ab:c5:ea:30:5c:69:05:11:21:33:33:a5:f9.
Are you sure you want to continue connecting (yes/no)? yes
Warning: Permanently added 'slave2,192.168.218.223' (ECDSA) to the list of known hosts.
Last login: Mon Mar 16 03:33:42 2020 from master
[root@slave2 ~]# logout
Connection to slave2 closed.
```

### 验证slave2

```bash
[root@slave2 .ssh]# ssh master
Last login: Mon Mar 16 03:35:08 2020 from slave1
[root@master ~]# logout
Connection to master closed.
[root@slave2 .ssh]# ssh slave1
The authenticity of host 'slave1 (192.168.218.222)' can't be established.
ECDSA key fingerprint is SHA256:7ZoGa2CvtrbbQKlBTyKoRuGEeCFFsM18OxSN5xh0fG4.
ECDSA key fingerprint is MD5:6a:99:5a:59:c6:8a:af:57:d7:a5:65:ca:8b:bd:a1:1b.
Are you sure you want to continue connecting (yes/no)? yes
Warning: Permanently added 'slave1,192.168.218.222' (ECDSA) to the list of known hosts.
Last login: Mon Mar 16 03:35:17 2020 from slave1
[root@slave1 ~]# logout
Connection to slave1 closed.
[root@slave2 .ssh]# ssh slave2
The authenticity of host 'slave2 (192.168.218.223)' can't be established.
ECDSA key fingerprint is SHA256:suTPXLIGxNCh2JEseJ7xhYe/7bEUxbsgsavdR3/u4C4.
ECDSA key fingerprint is MD5:b1:89:31:ab:c5:ea:30:5c:69:05:11:21:33:33:a5:f9.
Are you sure you want to continue connecting (yes/no)? yes
Warning: Permanently added 'slave2,192.168.218.223' (ECDSA) to the list of known hosts.
Last login: Mon Mar 16 03:35:27 2020 from slave1
[root@slave2 ~]# logout
Connection to slave2 closed.
```

## 设置完之后，停止并提交

```bash
[root@docker ~]# docker container stop master
master
[root@docker ~]# docker container stop slave1
slave1
[root@docker ~]# docker container stop slave2
slave2
[root@docker ~]# docker container commit master centos:master_v1.0
sha256:385bf9392bfcf601cfbd198357ae0e9bc8961c12ac3ac3fcdda5df14d7160f55
[root@docker ~]# docker container commit slave1 centos:slave1_v1.0
sha256:c5e80605e4b297d2df72033a05e9a3cd0d826028ae88cdf85d59dfc64dd9fa5b
[root@docker ~]# docker container commit slave2 centos:slave2_v1.0
sha256:eeb28ba87df9dade64e226e95b088baa22c0253154949e6faa5a20a188bf3d19
```

## 关闭容器后，重启问题

关闭master，slave1，slave2容器之后，重新启动对应的容器，会发现sshd服务关闭掉了，因此ssh服务暂时也无法使用，暂时没找到更好的解决办法，只能在每个容器中执行如下命令：

```bash
[root@slave1 /]# /usr/sbin/sshd
WARNING: 'UsePAM no' is not supported in Red Hat Enterprise Linux and may cause several problems.
[root@slave1 /]# ps -ef | grep ssh
root         32      0  0 05:24 ?        00:00:00 /usr/sbin/sshd
root         34     16  0 05:24 pts/1    00:00:00 grep --color=auto ssh
```

## 使用容器提交的镜像，启动新的容器验证ssh

```bash
[root@docker docker]# docker run -itd --name master -h master --add-host master:192.168.218.221 --add-host slave1:192.168.218.222 --add-host slave2:192.168.218.223 --net=mynetwork --ip=192.168.218.221 centos:master_v1.0
5632a11076638242a360c03978424b6478d765cd186d0adcbac23849c0daf8cd
[root@docker docker]# docker run -itd --name slave1 -h slave1 --add-host master:192.168.218.221 --add-host slave1:192.168.218.222 --add-host slave2:192.168.218.223 --net=mynetwork --ip=192.168.218.222 centos:slave1_v1.0
b24cd000df46daa03b2b6ddf45e5a6334b5bac637e5d860bcfad4b57743ce80a
[root@docker docker]# docker run -itd --name slave2 -h slave2 --add-host master:192.168.218.221 --add-host slave1:192.168.218.222 --add-host slave2:192.168.218.223 --net=mynetwork --ip=192.168.218.223 centos:slave2_v1.0
07cd9f8c2c49f2c3ee82a27a936dda5e917b783a1fe925a1f5f245b3e92e5d63

// 分别验证，确认可以互信
[root@docker ~]# docker exec -it master /bin/bash
// 因为遗留问题，暂时需要手动启动sshd，在后面操作中已经修复为自动启动了
[root@master /]# /usr/sbin/sshd
WARNING: 'UsePAM no' is not supported in Red Hat Enterprise Linux and may cause several problems.
[root@master /]# ssh master
Last login: Mon Mar 16 03:37:32 2020 from slave2
[root@master ~]# logout
Connection to master closed.
[root@master /]# ssh slave1
Last login: Mon Mar 16 03:45:03 2020 from slave2
[root@slave1 ~]# logout
Connection to slave1 closed.
```

## 思路设计

分为三个文件夹制作Dockerfile，每个Dockerfile生成一个镜像，只负责将软件拷贝到软件中，并且解压，还有就是将已经整理好的配置文件拷贝到对应的软件包文件夹中，根据实际操作和验证不断更新配置文件，目前集群的版本已经能满足大部分要求了。

### Dockerfile文件制作

首先，是软件的解压安装，因为docker的COPY以及ADD指令都只能在Docker的对应目录下控制文件的移动，因此，使用一个脚本来控制文件的移动和镜像构建。

步骤1：将所有的软件都放到/opt/cluster_soft中

步骤2：在每一个文件夹下编写build_xxx.sh文件，文件内容：

```bash
[root@docker master]# cat build_master.sh
#!/bin/bash

cp -r /opt/cluster_soft/ /opt/docker/master/

docker image build -t centos:master_v1.3 .
```

其他两个文件，和该文件大致一样，具体可以参考对应的脚本文件。

步骤3：

分别编写对应的Dockerfile文件，然后分别执行不同的build_xxx.sh脚本文件。

步骤4：

在所有的任务都编辑完成之后，需要删除对应的软件，减少服务器的磁盘占用，已经写到Dockerfile中。

## 构建结果测试

### scala、java、zookeeper对应测试

以上三者构建之后，需要对三者进行验证。

首先按照新构建的镜像，启动对应的新容器：

```bash
[root@docker docker]# docker run -itd --name master -h master --add-host master:192.168.218.221 --add-host slave1:192.168.218.222 --add-host slave2:192.168.218.223 --net=mynetwork --ip=192.168.218.221 centos:master_v1.2 && docker exec -it master /bin/bash
...
[root@docker docker]# docker run -itd --name slave1 -h slave1 --add-host master:192.168.218.221 --add-host slave1:192.168.218.222 --add-host slave2:192.168.218.223 --net=mynetwork --ip=192.168.218.222 centos:slave1_v1.2 && docker exec -it slave1 /bin/bash
...
[root@docker docker]# docker run -itd --name slave2 -h slave2 --add-host master:192.168.218.221 --add-host slave1:192.168.218.222 --add-host slave2:192.168.218.223 --net=mynetwork --ip=192.168.218.223 centos:slave2_v1.2 && docker exec -it slave2 /bin/bash
...
```

然后分别在三个容器里面执行/usr/sbin/sshd，启动zookeeper，效果如下所示：

![zookeeper集群用dockerfile部署效果](Docker搭建大数据组件集群的全过程/zookeeper集群用dockerfile部署效果.png)

zookeeper开放的端口为2888，3888，后续如果要进行开发，可能需要对宿主机进行映射。[待定]

### hadoop配置文件修改拷贝到宿主机

测试完zookeeper之后，我们发现其实可以通过hostname来直接对应服务的配置文件涉及到的host，以此来取代每次都写IP的繁琐，因此我们直接在上述测试的容器中修改hadoop的配置文件，然后进行hadoop namenode -format，并启动相关的服务，当成功之后，将配置文件拷贝到宿主机对应的文件夹中，反向来进行dockerfile编写。

#### 修改hadoop-env.sh

该文件位于hadoop目录下的etc/hadoop目录下，找到export JAVA_HOME=${JAVA_HOME}这一行，修改为：

```bash
export JAVA_HOME=/usr/local/java
```

#### 修改slaves文件

该文件位于hadoop目录下的etc/hadoop目录下，覆盖slaves内容，覆盖后内容为：

```bash
slave1
slave2
```

#### 修改core-site.xml文件

该文件位于hadoop目录下的etc/hadoop目录下，覆盖内容为：

```xml
<?xml version="1.0" encoding="UTF-8"?>
<?xml-stylesheet type="text/xsl" href="configuration.xsl"?>
<!--
  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License. See accompanying LICENSE file.
-->

<!-- Put site-specific property overrides in this file. -->

<configuration>
    <property>
        <name>hadoop.tmp.dir</name>
        <value>/root/hadoop/tmp</value>
    </property>
    <property>
        <name>fs.default.name</name>
        <value>hdfs://master:9000</value>
    </property>
</configuration>
```

#### 修改hdfs-site.xml

该文件位于hadoop目录下的etc/hadoop目录下，覆盖内容为：

```xml
<?xml version="1.0" encoding="UTF-8"?>
<?xml-stylesheet type="text/xsl" href="configuration.xsl"?>
<!--
  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License. See accompanying LICENSE file.
-->

<!-- Put site-specific property overrides in this file. -->

<configuration>
    <property>
        <name>dfs.name.dir</name>
        <value>/root/hadoop/dfs/name</value>
    </property>
    <property>
        <name>dfs.data.dir</name>
        <value>/root/hadoop/dfs/data</value>
    </property>
    <property>
        <name>dfs.permissions</name>
        <value>false</value>
    </property>
    <property>
        <name>dfs.replication</name>
        <value>2</value>
    </property>
</configuration>
```

#### 修改mapred-site.xml文件

该文件位于hadoop目录下的etc/hadoop目录下，覆盖内容为：

```xml
<?xml version="1.0"?>
<?xml-stylesheet type="text/xsl" href="configuration.xsl"?>
<!--
  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License. See accompanying LICENSE file.
-->

<!-- Put site-specific property overrides in this file. -->

<configuration>
    <property>
        <name>mapred.job.tracker</name>
        <value>master:49001</value>
    </property>
    <property>
        <name>mapred.local.dir</name>
        <value>/root/hadoop/var</value>
    </property>
    <property>
        <name>mapreduce.framework.name</name>
        <value>yarn</value>
    </property>
    <property>
        <name>mapreduce.jobhistory.address</name>
        <value>master:10020</value>
    </property>
    <property>
        <name>mapreduce.jobhistory.webapp.address</name>
        <value>master:19888</value>
    </property>
</configuration>
```

#### 修改yarn-site.xml

该文件位于hadoop目录下的etc/hadoop目录下，覆盖内容为：

```xml
<?xml version="1.0"?>
<!--
  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License. See accompanying LICENSE file.
-->
<configuration>

<!-- Site specific YARN configuration properties -->
    <property>
        <name>yarn.resourcemanager.hostname</name>
        <value>master</value>
    </property>
    <property>
        <description>The address of the applications manager interface in the RM.</description>
        <name>yarn.resourcemanager.address</name>
        <value>${yarn.resourcemanager.hostname}:8032</value>
    </property>
    <property>
        <description>The address of the scheduler interface.</description>
        <name>yarn.resourcemanager.scheduler.address</name>
        <value>${yarn.resourcemanager.hostname}:8030</value>
    </property>
    <property>
        <description>The http address of the RM web application.</description>
        <name>yarn.resourcemanager.webapp.address</name>
        <value>${yarn.resourcemanager.hostname}:8088</value>
    </property>
    <property>
        <description>The https adddress of the RM web application.</description>
        <name>yarn.resourcemanager.webapp.https.address</name>
        <value>${yarn.resourcemanager.hostname}:8090</value>
    </property>
    <property>
        <name>yarn.resourcemanager.resource-tracker.address</name>
        <value>${yarn.resourcemanager.hostname}:8031</value>
    </property>
    <property>
        <description>The address of the RM admin interface.</description>
        <name>yarn.resourcemanager.admin.address</name>
        <value>${yarn.resourcemanager.hostname}:8033</value>
    </property>
    <property>
        <name>yarn.nodemanager.aux-services</name>
        <value>mapreduce_shuffle</value>
    </property>
    <property>
        <name>yarn.scheduler.maximum-allocation-mb</name>
        <value>2048</value>
        <discription>8182MB</discription>
    </property>
    <property>
        <name>yarn.nodemanager.vmem-pmem-ratio</name>
        <value>2.1</value>
    </property>
    <property>
        <name>yarn.nodemanager.resource.memory-mb</name>
        <value>2048</value>
    </property>
    <property>
        <name>yarn.log-aggregation-enable</name>
        <value>true</value>
    </property>
    <property>
        <name>yarn.log-aggregation.retain-seconds</name>
        <value>86400</value>
    </property>
    <property>
        <name>yarn.nodemanager.vmem-check-enabled</name>
        <value>false</value>
    </property>
</configuration>
```

### 创建文件夹

master节点：

```bash
mkdir /root/hadoop && mkdir /root/hadoop/tmp && mkdir /root/hadoop/var && mkdir /root/hadoop/dfs && mkdir /root/hadoop/dfs/name && mkdir /root/hadoop/dfs/data
```

slave1节点：

```bash
mkdir /root/hadoop && mkdir /root/hadoop/tmp && mkdir /root/hadoop/var && mkdir /root/hadoop/dfs && mkdir /root/hadoop/dfs/name && mkdir /root/hadoop/dfs/data && mkdir /root/hadoop/dfs/data/slave1
```

slave2节点：

```bash
mkdir /root/hadoop && mkdir /root/hadoop/tmp && mkdir /root/hadoop/var && mkdir /root/hadoop/dfs && mkdir /root/hadoop/dfs/name && mkdir /root/hadoop/dfs/data && mkdir /root/hadoop/dfs/data/slave2
```

### 执行初始化

```bash
[root@master logs]# /usr/local/hadoop/bin/hadoop namenode -format
DEPRECATED: Use of this script to execute hdfs command is deprecated.
Instead use the hdfs command for it.

20/03/16 12:45:25 INFO namenode.NameNode: STARTUP_MSG:
/************************************************************
STARTUP_MSG: Starting NameNode
STARTUP_MSG:   user = root
STARTUP_MSG:   host = master/192.168.218.221
STARTUP_MSG:   args = [-format]
STARTUP_MSG:   version = 2.8.5
STARTUP_MSG:   classpath = /usr/local/hadoop/etc/hadoop:/usr/local/hadoop/share/hadoop/common/lib/activation-1.1.jar:/usr/local/hadoop/share/hadoop/common/lib/apacheds-i18n-2.0.0-M15.jar:/usr/local/hadoop/share/hadoop/common/lib/apacheds-kerberos-codec-2.0.0-M15.jar:/usr/local/hadoop/share/hadoop/common/lib/api-asn1-api-1.0.0-M20.jar:/usr/local/hadoop/share/hadoop/common/lib/api-util-1.0.0-M20.jar:/usr/local/hadoop/share/hadoop/common/lib/asm-3.2.jar:/usr/local/hadoop/share/hadoop/common/lib/avro-1.7.4.jar:/usr/local/hadoop/share/hadoop/common/lib/commons-beanutils-1.7.0.jar:/usr/local/hadoop/share/hadoop/common/lib/commons-beanutils-core-1.8.0.jar:/usr/local/hadoop/share/hadoop/common/lib/commons-cli-1.2.jar:/usr/local/hadoop/share/hadoop/common/lib/commons-codec-1.4.jar:/usr/local/hadoop/share/hadoop/common/lib/commons-collections-3.2.2.jar:/usr/local/hadoop/share/hadoop/common/lib/commons-compress-1.4.1.jar:/usr/local/hadoop/share/hadoop/common/lib/commons-configuration-1.6.jar:/usr/local/hadoop/share/hadoop/common/lib/commons-digester-1.8.jar:/usr/local/hadoop/share/hadoop/common/lib/commons-io-2.4.jar:/usr/local/hadoop/share/hadoop/common/lib/commons-lang-2.6.jar:/usr/local/hadoop/share/hadoop/common/lib/commons-logging-1.1.3.jar:/usr/local/hadoop/share/hadoop/common/lib/commons-math3-3.1.1.jar:/usr/local/hadoop/share/hadoop/common/lib/commons-net-3.1.jar:/usr/local/hadoop/share/hadoop/common/lib/curator-client-2.7.1.jar:/usr/local/hadoop/share/hadoop/common/lib/curator-framework-2.7.1.jar:/usr/local/hadoop/share/hadoop/common/lib/curator-recipes-2.7.1.jar:/usr/local/hadoop/share/hadoop/common/lib/gson-2.2.4.jar:/usr/local/hadoop/share/hadoop/common/lib/guava-11.0.2.jar:/usr/local/hadoop/share/hadoop/common/lib/hadoop-annotations-2.8.5.jar:/usr/local/hadoop/share/hadoop/common/lib/hadoop-auth-2.8.5.jar:/usr/local/hadoop/share/hadoop/common/lib/hamcrest-core-1.3.jar:/usr/local/hadoop/share/hadoop/common/lib/htrace-core4-4.0.1-incubating.jar:/usr/local/hadoop/share/hadoop/common/lib/httpclient-4.5.2.jar:/usr/local/hadoop/share/hadoop/common/lib/httpcore-4.4.4.jar:/usr/local/hadoop/share/hadoop/common/lib/jackson-core-asl-1.9.13.jar:/usr/local/hadoop/share/hadoop/common/lib/jackson-jaxrs-1.9.13.jar:/usr/local/hadoop/share/hadoop/common/lib/jackson-mapper-asl-1.9.13.jar:/usr/local/hadoop/share/hadoop/common/lib/jackson-xc-1.9.13.jar:/usr/local/hadoop/share/hadoop/common/lib/java-xmlbuilder-0.4.jar:/usr/local/hadoop/share/hadoop/common/lib/jaxb-api-2.2.2.jar:/usr/local/hadoop/share/hadoop/common/lib/jaxb-impl-2.2.3-1.jar:/usr/local/hadoop/share/hadoop/common/lib/jcip-annotations-1.0-1.jar:/usr/local/hadoop/share/hadoop/common/lib/jersey-core-1.9.jar:/usr/local/hadoop/share/hadoop/common/lib/jersey-json-1.9.jar:/usr/local/hadoop/share/hadoop/common/lib/jersey-server-1.9.jar:/usr/local/hadoop/share/hadoop/common/lib/jets3t-0.9.0.jar:/usr/local/hadoop/share/hadoop/common/lib/jettison-1.1.jar:/usr/local/hadoop/share/hadoop/common/lib/jetty-6.1.26.jar:/usr/local/hadoop/share/hadoop/common/lib/jetty-sslengine-6.1.26.jar:/usr/local/hadoop/share/hadoop/common/lib/jetty-util-6.1.26.jar:/usr/local/hadoop/share/hadoop/common/lib/jsch-0.1.54.jar:/usr/local/hadoop/share/hadoop/common/lib/json-smart-1.3.1.jar:/usr/local/hadoop/share/hadoop/common/lib/jsp-api-2.1.jar:/usr/local/hadoop/share/hadoop/common/lib/jsr305-3.0.0.jar:/usr/local/hadoop/share/hadoop/common/lib/junit-4.11.jar:/usr/local/hadoop/share/hadoop/common/lib/log4j-1.2.17.jar:/usr/local/hadoop/share/hadoop/common/lib/mockito-all-1.8.5.jar:/usr/local/hadoop/share/hadoop/common/lib/netty-3.6.2.Final.jar:/usr/local/hadoop/share/hadoop/common/lib/nimbus-jose-jwt-4.41.1.jar:/usr/local/hadoop/share/hadoop/common/lib/paranamer-2.3.jar:/usr/local/hadoop/share/hadoop/common/lib/protobuf-java-2.5.0.jar:/usr/local/hadoop/share/hadoop/common/lib/servlet-api-2.5.jar:/usr/local/hadoop/share/hadoop/common/lib/slf4j-api-1.7.10.jar:/usr/local/hadoop/share/hadoop/common/lib/slf4j-log4j12-1.7.10.jar:/usr/local/hadoop/share/hadoop/common/lib/snappy-java-1.0.4.1.jar:/usr/local/hadoop/share/hadoop/common/lib/stax-api-1.0-2.jar:/usr/local/hadoop/share/hadoop/common/lib/xmlenc-0.52.jar:/usr/local/hadoop/share/hadoop/common/lib/xz-1.0.jar:/usr/local/hadoop/share/hadoop/common/lib/zookeeper-3.4.6.jar:/usr/local/hadoop/share/hadoop/common/hadoop-common-2.8.5-tests.jar:/usr/local/hadoop/share/hadoop/common/hadoop-common-2.8.5.jar:/usr/local/hadoop/share/hadoop/common/hadoop-nfs-2.8.5.jar:/usr/local/hadoop/share/hadoop/hdfs:/usr/local/hadoop/share/hadoop/hdfs/lib/asm-3.2.jar:/usr/local/hadoop/share/hadoop/hdfs/lib/commons-cli-1.2.jar:/usr/local/hadoop/share/hadoop/hdfs/lib/commons-codec-1.4.jar:/usr/local/hadoop/share/hadoop/hdfs/lib/commons-daemon-1.0.13.jar:/usr/local/hadoop/share/hadoop/hdfs/lib/commons-io-2.4.jar:/usr/local/hadoop/share/hadoop/hdfs/lib/commons-lang-2.6.jar:/usr/local/hadoop/share/hadoop/hdfs/lib/commons-logging-1.1.3.jar:/usr/local/hadoop/share/hadoop/hdfs/lib/guava-11.0.2.jar:/usr/local/hadoop/share/hadoop/hdfs/lib/hadoop-hdfs-client-2.8.5.jar:/usr/local/hadoop/share/hadoop/hdfs/lib/htrace-core4-4.0.1-incubating.jar:/usr/local/hadoop/share/hadoop/hdfs/lib/jackson-core-asl-1.9.13.jar:/usr/local/hadoop/share/hadoop/hdfs/lib/jackson-mapper-asl-1.9.13.jar:/usr/local/hadoop/share/hadoop/hdfs/lib/jersey-core-1.9.jar:/usr/local/hadoop/share/hadoop/hdfs/lib/jersey-server-1.9.jar:/usr/local/hadoop/share/hadoop/hdfs/lib/jetty-6.1.26.jar:/usr/local/hadoop/share/hadoop/hdfs/lib/jetty-util-6.1.26.jar:/usr/local/hadoop/share/hadoop/hdfs/lib/jsr305-3.0.0.jar:/usr/local/hadoop/share/hadoop/hdfs/lib/leveldbjni-all-1.8.jar:/usr/local/hadoop/share/hadoop/hdfs/lib/log4j-1.2.17.jar:/usr/local/hadoop/share/hadoop/hdfs/lib/netty-3.6.2.Final.jar:/usr/local/hadoop/share/hadoop/hdfs/lib/netty-all-4.0.23.Final.jar:/usr/local/hadoop/share/hadoop/hdfs/lib/okhttp-2.4.0.jar:/usr/local/hadoop/share/hadoop/hdfs/lib/okio-1.4.0.jar:/usr/local/hadoop/share/hadoop/hdfs/lib/protobuf-java-2.5.0.jar:/usr/local/hadoop/share/hadoop/hdfs/lib/servlet-api-2.5.jar:/usr/local/hadoop/share/hadoop/hdfs/lib/xercesImpl-2.9.1.jar:/usr/local/hadoop/share/hadoop/hdfs/lib/xml-apis-1.3.04.jar:/usr/local/hadoop/share/hadoop/hdfs/lib/xmlenc-0.52.jar:/usr/local/hadoop/share/hadoop/hdfs/hadoop-hdfs-2.8.5-tests.jar:/usr/local/hadoop/share/hadoop/hdfs/hadoop-hdfs-2.8.5.jar:/usr/local/hadoop/share/hadoop/hdfs/hadoop-hdfs-client-2.8.5-tests.jar:/usr/local/hadoop/share/hadoop/hdfs/hadoop-hdfs-client-2.8.5.jar:/usr/local/hadoop/share/hadoop/hdfs/hadoop-hdfs-native-client-2.8.5-tests.jar:/usr/local/hadoop/share/hadoop/hdfs/hadoop-hdfs-native-client-2.8.5.jar:/usr/local/hadoop/share/hadoop/hdfs/hadoop-hdfs-nfs-2.8.5.jar:/usr/local/hadoop/share/hadoop/yarn/lib/activation-1.1.jar:/usr/local/hadoop/share/hadoop/yarn/lib/aopalliance-1.0.jar:/usr/local/hadoop/share/hadoop/yarn/lib/asm-3.2.jar:/usr/local/hadoop/share/hadoop/yarn/lib/commons-cli-1.2.jar:/usr/local/hadoop/share/hadoop/yarn/lib/commons-codec-1.4.jar:/usr/local/hadoop/share/hadoop/yarn/lib/commons-collections-3.2.2.jar:/usr/local/hadoop/share/hadoop/yarn/lib/commons-compress-1.4.1.jar:/usr/local/hadoop/share/hadoop/yarn/lib/commons-io-2.4.jar:/usr/local/hadoop/share/hadoop/yarn/lib/commons-lang-2.6.jar:/usr/local/hadoop/share/hadoop/yarn/lib/commons-logging-1.1.3.jar:/usr/local/hadoop/share/hadoop/yarn/lib/commons-math-2.2.jar:/usr/local/hadoop/share/hadoop/yarn/lib/curator-client-2.7.1.jar:/usr/local/hadoop/share/hadoop/yarn/lib/curator-test-2.7.1.jar:/usr/local/hadoop/share/hadoop/yarn/lib/fst-2.50.jar:/usr/local/hadoop/share/hadoop/yarn/lib/guava-11.0.2.jar:/usr/local/hadoop/share/hadoop/yarn/lib/guice-3.0.jar:/usr/local/hadoop/share/hadoop/yarn/lib/guice-servlet-3.0.jar:/usr/local/hadoop/share/hadoop/yarn/lib/jackson-core-asl-1.9.13.jar:/usr/local/hadoop/share/hadoop/yarn/lib/jackson-jaxrs-1.9.13.jar:/usr/local/hadoop/share/hadoop/yarn/lib/jackson-mapper-asl-1.9.13.jar:/usr/local/hadoop/share/hadoop/yarn/lib/jackson-xc-1.9.13.jar:/usr/local/hadoop/share/hadoop/yarn/lib/java-util-1.9.0.jar:/usr/local/hadoop/share/hadoop/yarn/lib/javassist-3.18.1-GA.jar:/usr/local/hadoop/share/hadoop/yarn/lib/javax.inject-1.jar:/usr/local/hadoop/share/hadoop/yarn/lib/jaxb-api-2.2.2.jar:/usr/local/hadoop/share/hadoop/yarn/lib/jaxb-impl-2.2.3-1.jar:/usr/local/hadoop/share/hadoop/yarn/lib/jersey-client-1.9.jar:/usr/local/hadoop/share/hadoop/yarn/lib/jersey-core-1.9.jar:/usr/local/hadoop/share/hadoop/yarn/lib/jersey-guice-1.9.jar:/usr/local/hadoop/share/hadoop/yarn/lib/jersey-json-1.9.jar:/usr/local/hadoop/share/hadoop/yarn/lib/jersey-server-1.9.jar:/usr/local/hadoop/share/hadoop/yarn/lib/jettison-1.1.jar:/usr/local/hadoop/share/hadoop/yarn/lib/jetty-6.1.26.jar:/usr/local/hadoop/share/hadoop/yarn/lib/jetty-util-6.1.26.jar:/usr/local/hadoop/share/hadoop/yarn/lib/json-io-2.5.1.jar:/usr/local/hadoop/share/hadoop/yarn/lib/jsr305-3.0.0.jar:/usr/local/hadoop/share/hadoop/yarn/lib/leveldbjni-all-1.8.jar:/usr/local/hadoop/share/hadoop/yarn/lib/log4j-1.2.17.jar:/usr/local/hadoop/share/hadoop/yarn/lib/netty-3.6.2.Final.jar:/usr/local/hadoop/share/hadoop/yarn/lib/protobuf-java-2.5.0.jar:/usr/local/hadoop/share/hadoop/yarn/lib/servlet-api-2.5.jar:/usr/local/hadoop/share/hadoop/yarn/lib/stax-api-1.0-2.jar:/usr/local/hadoop/share/hadoop/yarn/lib/xz-1.0.jar:/usr/local/hadoop/share/hadoop/yarn/lib/zookeeper-3.4.6-tests.jar:/usr/local/hadoop/share/hadoop/yarn/lib/zookeeper-3.4.6.jar:/usr/local/hadoop/share/hadoop/yarn/hadoop-yarn-api-2.8.5.jar:/usr/local/hadoop/share/hadoop/yarn/hadoop-yarn-applications-distributedshell-2.8.5.jar:/usr/local/hadoop/share/hadoop/yarn/hadoop-yarn-applications-unmanaged-am-launcher-2.8.5.jar:/usr/local/hadoop/share/hadoop/yarn/hadoop-yarn-client-2.8.5.jar:/usr/local/hadoop/share/hadoop/yarn/hadoop-yarn-common-2.8.5.jar:/usr/local/hadoop/share/hadoop/yarn/hadoop-yarn-registry-2.8.5.jar:/usr/local/hadoop/share/hadoop/yarn/hadoop-yarn-server-applicationhistoryservice-2.8.5.jar:/usr/local/hadoop/share/hadoop/yarn/hadoop-yarn-server-common-2.8.5.jar:/usr/local/hadoop/share/hadoop/yarn/hadoop-yarn-server-nodemanager-2.8.5.jar:/usr/local/hadoop/share/hadoop/yarn/hadoop-yarn-server-resourcemanager-2.8.5.jar:/usr/local/hadoop/share/hadoop/yarn/hadoop-yarn-server-sharedcachemanager-2.8.5.jar:/usr/local/hadoop/share/hadoop/yarn/hadoop-yarn-server-tests-2.8.5.jar:/usr/local/hadoop/share/hadoop/yarn/hadoop-yarn-server-timeline-pluginstorage-2.8.5.jar:/usr/local/hadoop/share/hadoop/yarn/hadoop-yarn-server-web-proxy-2.8.5.jar:/usr/local/hadoop/share/hadoop/mapreduce/lib/aopalliance-1.0.jar:/usr/local/hadoop/share/hadoop/mapreduce/lib/asm-3.2.jar:/usr/local/hadoop/share/hadoop/mapreduce/lib/avro-1.7.4.jar:/usr/local/hadoop/share/hadoop/mapreduce/lib/commons-compress-1.4.1.jar:/usr/local/hadoop/share/hadoop/mapreduce/lib/commons-io-2.4.jar:/usr/local/hadoop/share/hadoop/mapreduce/lib/guice-3.0.jar:/usr/local/hadoop/share/hadoop/mapreduce/lib/guice-servlet-3.0.jar:/usr/local/hadoop/share/hadoop/mapreduce/lib/hadoop-annotations-2.8.5.jar:/usr/local/hadoop/share/hadoop/mapreduce/lib/hamcrest-core-1.3.jar:/usr/local/hadoop/share/hadoop/mapreduce/lib/jackson-core-asl-1.9.13.jar:/usr/local/hadoop/share/hadoop/mapreduce/lib/jackson-mapper-asl-1.9.13.jar:/usr/local/hadoop/share/hadoop/mapreduce/lib/javax.inject-1.jar:/usr/local/hadoop/share/hadoop/mapreduce/lib/jersey-core-1.9.jar:/usr/local/hadoop/share/hadoop/mapreduce/lib/jersey-guice-1.9.jar:/usr/local/hadoop/share/hadoop/mapreduce/lib/jersey-server-1.9.jar:/usr/local/hadoop/share/hadoop/mapreduce/lib/junit-4.11.jar:/usr/local/hadoop/share/hadoop/mapreduce/lib/leveldbjni-all-1.8.jar:/usr/local/hadoop/share/hadoop/mapreduce/lib/log4j-1.2.17.jar:/usr/local/hadoop/share/hadoop/mapreduce/lib/netty-3.6.2.Final.jar:/usr/local/hadoop/share/hadoop/mapreduce/lib/paranamer-2.3.jar:/usr/local/hadoop/share/hadoop/mapreduce/lib/protobuf-java-2.5.0.jar:/usr/local/hadoop/share/hadoop/mapreduce/lib/snappy-java-1.0.4.1.jar:/usr/local/hadoop/share/hadoop/mapreduce/lib/xz-1.0.jar:/usr/local/hadoop/share/hadoop/mapreduce/hadoop-mapreduce-client-app-2.8.5.jar:/usr/local/hadoop/share/hadoop/mapreduce/hadoop-mapreduce-client-common-2.8.5.jar:/usr/local/hadoop/share/hadoop/mapreduce/hadoop-mapreduce-client-core-2.8.5.jar:/usr/local/hadoop/share/hadoop/mapreduce/hadoop-mapreduce-client-hs-2.8.5.jar:/usr/local/hadoop/share/hadoop/mapreduce/hadoop-mapreduce-client-hs-plugins-2.8.5.jar:/usr/local/hadoop/share/hadoop/mapreduce/hadoop-mapreduce-client-jobclient-2.8.5-tests.jar:/usr/local/hadoop/share/hadoop/mapreduce/hadoop-mapreduce-client-jobclient-2.8.5.jar:/usr/local/hadoop/share/hadoop/mapreduce/hadoop-mapreduce-client-shuffle-2.8.5.jar:/usr/local/hadoop/share/hadoop/mapreduce/hadoop-mapreduce-examples-2.8.5.jar:/usr/local/hadoop/contrib/capacity-scheduler/*.jar:/usr/local/hadoop/contrib/capacity-scheduler/*.jar
STARTUP_MSG:   build = https://git-wip-us.apache.org/repos/asf/hadoop.git -r 0b8464d75227fcee2c6e7f2410377b3d53d3d5f8; compiled by 'jdu' on 2018-09-10T03:32Z
STARTUP_MSG:   java = 1.8.0_231
************************************************************/
20/03/16 12:45:25 INFO namenode.NameNode: registered UNIX signal handlers for [TERM, HUP, INT]
20/03/16 12:45:25 INFO namenode.NameNode: createNameNode [-format]
20/03/16 12:45:26 WARN common.Util: Path /root/hadoop/dfs/name should be specified as a URI in configuration files. Please update hdfs configuration.
20/03/16 12:45:26 WARN common.Util: Path /root/hadoop/dfs/name should be specified as a URI in configuration files. Please update hdfs configuration.
Formatting using clusterid: CID-fc48780e-0efb-403c-be45-2534e692c9d2
20/03/16 12:45:26 INFO namenode.FSEditLog: Edit logging is async:true
20/03/16 12:45:26 INFO namenode.FSNamesystem: KeyProvider: null
20/03/16 12:45:26 INFO namenode.FSNamesystem: fsLock is fair: true
20/03/16 12:45:26 INFO namenode.FSNamesystem: Detailed lock hold time metrics enabled: false
20/03/16 12:45:26 INFO blockmanagement.DatanodeManager: dfs.block.invalidate.limit=1000
20/03/16 12:45:26 INFO blockmanagement.DatanodeManager: dfs.namenode.datanode.registration.ip-hostname-check=true
20/03/16 12:45:26 INFO blockmanagement.BlockManager: dfs.namenode.startup.delay.block.deletion.sec is set to 000:00:00:00.000
20/03/16 12:45:26 INFO blockmanagement.BlockManager: The block deletion will start around 2020 Mar 16 12:45:26
20/03/16 12:45:26 INFO util.GSet: Computing capacity for map BlocksMap
20/03/16 12:45:26 INFO util.GSet: VM type       = 64-bit
20/03/16 12:45:26 INFO util.GSet: 2.0% max memory 889 MB = 17.8 MB
20/03/16 12:45:26 INFO util.GSet: capacity      = 2^21 = 2097152 entries
20/03/16 12:45:26 INFO blockmanagement.BlockManager: dfs.block.access.token.enable=false
20/03/16 12:45:26 INFO blockmanagement.BlockManager: defaultReplication         = 2
20/03/16 12:45:26 INFO blockmanagement.BlockManager: maxReplication             = 512
20/03/16 12:45:26 INFO blockmanagement.BlockManager: minReplication             = 1
20/03/16 12:45:26 INFO blockmanagement.BlockManager: maxReplicationStreams      = 2
20/03/16 12:45:26 INFO blockmanagement.BlockManager: replicationRecheckInterval = 3000
20/03/16 12:45:26 INFO blockmanagement.BlockManager: encryptDataTransfer        = false
20/03/16 12:45:26 INFO blockmanagement.BlockManager: maxNumBlocksToLog          = 1000
20/03/16 12:45:26 INFO namenode.FSNamesystem: fsOwner             = root (auth:SIMPLE)
20/03/16 12:45:26 INFO namenode.FSNamesystem: supergroup          = supergroup
20/03/16 12:45:26 INFO namenode.FSNamesystem: isPermissionEnabled = false
20/03/16 12:45:26 INFO namenode.FSNamesystem: HA Enabled: false
20/03/16 12:45:26 INFO namenode.FSNamesystem: Append Enabled: true
20/03/16 12:45:26 INFO util.GSet: Computing capacity for map INodeMap
20/03/16 12:45:26 INFO util.GSet: VM type       = 64-bit
20/03/16 12:45:26 INFO util.GSet: 1.0% max memory 889 MB = 8.9 MB
20/03/16 12:45:26 INFO util.GSet: capacity      = 2^20 = 1048576 entries
20/03/16 12:45:26 INFO namenode.FSDirectory: ACLs enabled? false
20/03/16 12:45:26 INFO namenode.FSDirectory: XAttrs enabled? true
20/03/16 12:45:26 INFO namenode.NameNode: Caching file names occurring more than 10 times
20/03/16 12:45:26 INFO util.GSet: Computing capacity for map cachedBlocks
20/03/16 12:45:26 INFO util.GSet: VM type       = 64-bit
20/03/16 12:45:26 INFO util.GSet: 0.25% max memory 889 MB = 2.2 MB
20/03/16 12:45:26 INFO util.GSet: capacity      = 2^18 = 262144 entries
20/03/16 12:45:26 INFO namenode.FSNamesystem: dfs.namenode.safemode.threshold-pct = 0.9990000128746033
20/03/16 12:45:26 INFO namenode.FSNamesystem: dfs.namenode.safemode.min.datanodes = 0
20/03/16 12:45:26 INFO namenode.FSNamesystem: dfs.namenode.safemode.extension     = 30000
20/03/16 12:45:26 INFO metrics.TopMetrics: NNTop conf: dfs.namenode.top.window.num.buckets = 10
20/03/16 12:45:26 INFO metrics.TopMetrics: NNTop conf: dfs.namenode.top.num.users = 10
20/03/16 12:45:26 INFO metrics.TopMetrics: NNTop conf: dfs.namenode.top.windows.minutes = 1,5,25
20/03/16 12:45:26 INFO namenode.FSNamesystem: Retry cache on namenode is enabled
20/03/16 12:45:26 INFO namenode.FSNamesystem: Retry cache will use 0.03 of total heap and retry cache entry expiry time is 600000 millis
20/03/16 12:45:26 INFO util.GSet: Computing capacity for map NameNodeRetryCache
20/03/16 12:45:26 INFO util.GSet: VM type       = 64-bit
20/03/16 12:45:26 INFO util.GSet: 0.029999999329447746% max memory 889 MB = 273.1 KB
20/03/16 12:45:26 INFO util.GSet: capacity      = 2^15 = 32768 entries
20/03/16 12:45:26 INFO namenode.FSImage: Allocated new BlockPoolId: BP-1228238645-192.168.218.221-1584362726602
20/03/16 12:45:26 INFO common.Storage: Storage directory /root/hadoop/dfs/name has been successfully formatted.
20/03/16 12:45:26 INFO namenode.FSImageFormatProtobuf: Saving image file /root/hadoop/dfs/name/current/fsimage.ckpt_0000000000000000000 using no compression
20/03/16 12:45:26 INFO namenode.FSImageFormatProtobuf: Image file /root/hadoop/dfs/name/current/fsimage.ckpt_0000000000000000000 of size 320 bytes saved in 0 seconds.
20/03/16 12:45:26 INFO namenode.NNStorageRetentionManager: Going to retain 1 images with txid >= 0
20/03/16 12:45:26 INFO util.ExitUtil: Exiting with status 0
20/03/16 12:45:26 INFO namenode.NameNode: SHUTDOWN_MSG:
/************************************************************
SHUTDOWN_MSG: Shutting down NameNode at master/192.168.218.221
************************************************************/
```

运行start-dfs.sh和start-yarn.sh的效果图如下所示：

![hadoop集群用docker部署的结果图](Docker搭建大数据组件集群的全过程/hadoop集群用docker部署的结果图.png)

说明上述的配置暂时没错，因此将配置文件拷贝到宿主机上，因为hadoop三个节点的配置文件都是一样的，所以我们就将一台上的配置文件分别拷贝到本地宿主机上的各个文件夹中即可：

```bash
[root@docker master]# docker cp master:/usr/local/hadoop/etc/hadoop/hadoop-env.sh hadoop_config/
[root@docker master]# docker cp master:/usr/local/hadoop/etc/hadoop/slaves hadoop_config/
[root@docker master]# docker cp master:/usr/local/hadoop/etc/hadoop/hdfs-site.xml hadoop_config/
[root@docker master]# docker cp master:/usr/local/hadoop/etc/hadoop/mapred-site.xml hadoop_config/
[root@docker master]# docker cp master:/usr/local/hadoop/etc/hadoop/yarn-site.xml hadoop_config/
```

### spark配置文件修改拷贝到宿主机

spark主要需要修改的两个配置文件为conf/spark-env.sh，conf/slaves，另外为了开发方便，还需要修改spark-defaults.conf：

对应的文件内容这里就不贴出来了，因为spark的并不复杂，如有需要请直接看对应的文件：

在容器中启动的效果如下所示：

![spark集群用docker部署的结果](Docker搭建大数据组件集群的全过程/spark集群用docker部署的结果.png)

> 备注：后续会将hadoop和spark的部署内容几种写到各自节点的Dockerfile中，部署之后，将再写一个使用文档，简要介绍如何使用docker化的本集群。另外，每次使用之前，请使用上述启动集群容器化的方式，启动之后，然后执行对应的docker命令，后续会简单写到shell脚本中，可以使用该脚本直接启动docker大数据集群容器。

### 启动容器并设置一些端口映射

```bash
[root@docker docker]# docker run -itd --name master -h master -p 50070:50070 -p 50075:50075 -p 50475:50475 -p 9000:9000 -p 49001:49001 -p 19888:19888 -p 8032:8032 -p 8030:8030 -p 8088:8088 -p 8090:8090 -p 8031:8031 -p 8033:8033 -p 8080:8080 -p 8081:8081 -p 2181:2181 --add-host master:192.168.218.221 --add-host slave1:192.168.218.222 --add-host slave2:192.168.218.223 --net=mynetwork --ip=192.168.218.221 centos:master_v1.3
...
[root@docker docker]# docker run -itd --name slave1 -h slave1 -p 2182:2181 -p 8043:8042 --add-host master:192.168.218.221 --add-host slave1:192.168.218.222 --add-host slave2:192.168.218.223 --net=mynetwork --ip=192.168.218.222 centos:slave1_v1.3 && docker exec -it slave1 /bin/bash
...
[root@docker docker]# docker run -itd --name slave2 -h slave2 -p 2183:2181 -p 8044:8042 --add-host master:192.168.218.221 --add-host slave1:192.168.218.222 --add-host slave2:192.168.218.223 --net=mynetwork --ip=192.168.218.223 centos:slave2_v1.3 && docker exec -it slave2 /bin/bash
...
[root@docker docker]# docker exec -it master /bin/bash
```

### 集群组件常用的一些端口解释（为完善完全）

hadoop常用的一些端口：

```json
"PortBindings":{
    "50070/tcp":["HostIp":"","HostPort":"50070"],
    "50075/tcp":["HostIp":"","HostPort":"50075"],
    "50475/tcp":["HostIp":"","HostPort":"50475"],
    "9000/tcp":["HostIp":"","HostPort":"9000"],
    "49001/tcp":["HostIp":"","HostPort":"49001"],
    "19888/tcp":["HostIp":"","HostPort":"19888"],
    "8032/tcp":["HostIp":"","HostPort":"8032"],
    "8030/tcp":["HostIp":"","HostPort":"8030"],
    "8088/tcp":["HostIp":"","HostPort":"8088"],
    "8090/tcp":["HostIp":"","HostPort":"8090"],
    "8031/tcp":["HostIp":"","HostPort":"8031"],
    "8033/tcp":["HostIp":"","HostPort":"8033"]
}
```

spark常用的一些端口：

```bash
7077
8081
8080
```

zk常用的一些端口：

```bash
2181
```

slave1和slave2的8042端口也要映射一下，这边将slave1的8042端口映射到宿主机的8043端口，slave2的8042端口映射到宿主机的8044端口。