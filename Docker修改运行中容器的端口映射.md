<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
**Table of Contents**  *generated with [DocToc](https://github.com/thlorenz/doctoc)*

- [Docker修改运行中容器的端口映射](#docker%E4%BF%AE%E6%94%B9%E8%BF%90%E8%A1%8C%E4%B8%AD%E5%AE%B9%E5%99%A8%E7%9A%84%E7%AB%AF%E5%8F%A3%E6%98%A0%E5%B0%84)
  - [问题场景](#%E9%97%AE%E9%A2%98%E5%9C%BA%E6%99%AF)
  - [解决办法](#%E8%A7%A3%E5%86%B3%E5%8A%9E%E6%B3%95)
    - [方式一（最优解）](#%E6%96%B9%E5%BC%8F%E4%B8%80%E6%9C%80%E4%BC%98%E8%A7%A3)
    - [方式二](#%E6%96%B9%E5%BC%8F%E4%BA%8C)
    - [方式三](#%E6%96%B9%E5%BC%8F%E4%B8%89)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

# Docker修改运行中容器的端口映射

## 问题场景

在docker run创建并运行容器的时候，可以通过-p指定端口映射规则。但是有的时候会出现这样一种情况：

当你将容器运行起来之后，你会发现一些端口忘记映射了，比如，spark的webui端口忘记映射了，导致你无法在浏览器中访问到spark的ui界面，所以要解决这个问题场景，我们可以使用如下的方法。

## 解决办法

### 方式一（最优解）

这种方式是基于已创建的容器基础上，对端口映射进行添加，修改操作，启动容器后，容器中之前做的操作，依然保留，但是操作需要细致一些，避免出现错误，导致容器启动失败。

步骤1：执行docker ps -a找到要修改对应端口的容器；

步骤2：关闭容器中的服务，并关闭容器，最后执行命令systemctl stop docker将docker服务关闭（这点尤为重要，如果不关闭docker服务，则修改之后，修改内容依然会在启动容器后还原）；

步骤3：这里slave2为例子，slave2的container_id为0671ff5c114e172f35ae6e45f63c73071bc5b5002ef1e9d8b1f659d1736a8cce，进入到文件夹/var/lib/docker/containers/0671ff5c114e172f35ae6e45f63c73071bc5b5002ef1e9d8b1f659d1736a8cce，下面有这些文件：

```bash
[root@docker 0671ff5c114e172f35ae6e45f63c73071bc5b5002ef1e9d8b1f659d1736a8cce]# ll
total 28
-rw-r-----. 1 root root 1056 Mar 18 02:54 0671ff5c114e172f35ae6e45f63c73071bc5b5002ef1e9d8b1f659d1736a8cce-json.log
drwx------. 2 root root    6 Mar 17 23:29 checkpoints
-rw-------. 1 root root 3218 Mar 18 02:54 config.v2.json
-rw-r--r--. 1 root root 1633 Mar 18 02:54 hostconfig.json
-rw-r--r--. 1 root root    7 Mar 18 02:54 hostname
-rw-r--r--. 1 root root  242 Mar 18 02:54 hosts
drwx------. 2 root root    6 Mar 17 23:29 mounts
-rw-r--r--. 1 root root   52 Mar 18 02:54 resolv.conf
-rw-r--r--. 1 root root   71 Mar 18 02:54 resolv.conf.hash
```

步骤4：修改hostconfig.json文件，找到其中有一项PortBindings的服务项，将要映射的端口添加上去：

![修改hostconfig的json文件](Docker修改运行中容器的端口映射/修改hostconfig的json文件.png)

注意图中划线内容，可以见端口映射添加可以设置多个的，参照图中所示进行设置即可！

步骤5：经过验证，config.v2.json里面也记录了端口，因此接下来要修改config.v2.json文件：

![修改config.v2.json文件](Docker修改运行中容器的端口映射/修改config.v2.json文件.png)

注意图中划线内容，同样参照图中所示进行设置即可！

修改之后重启启动docker服务和容器，效果如下：

![修改之后的结果](Docker修改运行中容器的端口映射/修改之后的结果.png)

### 方式二

使用docker commit命令重新提交一层镜像，利用新的镜像去创建新的容器并且重新映射端口，这时候原来的东西还在，但是这种方式管理起来非常的混乱，不想方式一那么直观，可能将你原有的一些思路打乱，导致一些其他的细节问题。

### 方式三

删掉原来的容器，重新创建容器并映射未映射的端口，但是这样费时费力，并且应为没有像方式二一样，重新覆盖一层layer，导致将原先运行的容器中的东西全部抹去，一切都要重头再来。