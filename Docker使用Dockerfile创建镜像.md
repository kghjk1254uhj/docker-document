<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
**Table of Contents**  *generated with [DocToc](https://github.com/thlorenz/doctoc)*

- [Docker使用Dockerfile创建镜像](#docker%E4%BD%BF%E7%94%A8dockerfile%E5%88%9B%E5%BB%BA%E9%95%9C%E5%83%8F)
  - [基本结构](#%E5%9F%BA%E6%9C%AC%E7%BB%93%E6%9E%84)
  - [指令说明](#%E6%8C%87%E4%BB%A4%E8%AF%B4%E6%98%8E)
    - [配置指令](#%E9%85%8D%E7%BD%AE%E6%8C%87%E4%BB%A4)
      - [ARG](#arg)
      - [FROM](#from)
      - [LABEL](#label)
      - [EXPOSE](#expose)
      - [ENV](#env)
      - [ENTRYPOINT](#entrypoint)
      - [VOLUME](#volume)
      - [USER](#user)
      - [WORKDIR](#workdir)
      - [ONBUILD](#onbuild)
      - [STOPSIGNAL](#stopsignal)
      - [HEALTHCHECK](#healthcheck)
      - [SHELL](#shell)
    - [操作指令](#%E6%93%8D%E4%BD%9C%E6%8C%87%E4%BB%A4)
      - [RUN](#run)
      - [CMD](#cmd)
      - [ADD](#add)
      - [COPY](#copy)
  - [创建镜像](#%E5%88%9B%E5%BB%BA%E9%95%9C%E5%83%8F)
    - [命令选项](#%E5%91%BD%E4%BB%A4%E9%80%89%E9%A1%B9)
    - [选择父镜像](#%E9%80%89%E6%8B%A9%E7%88%B6%E9%95%9C%E5%83%8F)
    - [使用.dockerignore文件](#%E4%BD%BF%E7%94%A8dockerignore%E6%96%87%E4%BB%B6)
    - [多步骤创建](#%E5%A4%9A%E6%AD%A5%E9%AA%A4%E5%88%9B%E5%BB%BA)
  - [最佳实践](#%E6%9C%80%E4%BD%B3%E5%AE%9E%E8%B7%B5)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

# Docker使用Dockerfile创建镜像

Dockerfile是一个文本格式的配置文件，用户可以使用Dockerfile来快速创建自定义的镜像。

首先将介绍Dockerfile典型的基本结构及其支持的众多指令，并具体讲解通过这些指令来编写定制镜像的Dockerfile，以及如何生成镜像。最后，会介绍使用Dockerfile的一些最佳实践经验。

## 基本结构

Dockerfile由一行行命令语句组成，并且支持以\#开头的注释行。

一般而言，Dockerfile主体内容分为四部分：基础镜像信息、维护者信息、镜像操作指令和容器启动时执行指令。

下面给出一个简单的示例：

```bash
# escape=\ (backslash)
# This dockerfile uses the ubuntu:xeniel image
# VERSION 2- EDITION 1
# Author: docker_user
# Command format: Instruction [arguments / command] ..
# Base image to use, this must be set as the first line
FROM ubuntu:xeniel

# Maintainer: docker_user <docker_user at email.com> (@docker_user)
LABEL maintainer hchihping<23955637@qq.com>

# Commands to update the image
RUN echo "deb http://archive.ubuntu.com/ubuntu/ xeniel main universe" >> /etc/apt/sources.list
RUN apt-get update && apt-get install -y nginx
RUN echo "\ndaemon off;" >> /etc/nginx/nginx.conf
# Commands when creating a new container
CMD /usr/sbin/nginx
```

首行可以通过注释来指定解析器命令，后续通过注释说明镜像的相关信息。主体部分首先使用FROM指令指明所基于的镜像名称，接下来一般是使用LABEL指令说明维护者信息。后面则是镜像操作指令，例如RUN指令将对镜像执行跟随的命令。每运行一条RUN指令，镜像添加新的一层，并提交。最后是CMD指令，来指定运行容器时的操作命令。

```bash
FROM debian:jessie
MAINTAINER NGINX Docker Maintainers "docker-maint@nginx.com"
ENV NGINX_VERSION 1.10.1-1~jessie
RUN apt-key adv --keyserver hkp://pgp.mit.edu:80 --recv-keys 573BFD6B3D8FBC641079A6ABABF5BD827BD9BF62 \
       && echo "deb http://nginx.org/packages/debian/ jessie nginx" >> /etc/apt/sources.list \
       && apt-get update \
       && apt-get install --no-install-recommends --no-install-suggests -y \
       ca-certificates \
       nginx=${NGINX_VERSION} \
       nginx-module-xslt \
       nginx-module-geoip \
       nginx-module-image-filter \
       nginx-module-perl \
       nginx-module-njs \
       gettext-base \
       && rm -rf /var/lib/apt/lists/*
# forward request and error logs to docker log collector
RUN ln -sf /dev/stdout /var/log/nginx/access.log \
   && ln -sf /dev/stderr /var/log/nginx/error.log
EXPOSE 80 443
CMD ["nginx", "-g", "daemon off;"]
```

第二个是基于buildpack-deps:jessie-scm基础镜像，安装Golang相关环境，制作一个Go语言的运行环境镜像：

```bash
FROM buildpack-deps:jessie-scm
# gcc for cgo
RUN apt-get update && apt-get install -y --no-install-recommends \
   g++ \
   gcc \
   libc6-dev \
   make \
   && rm -rf /var/lib/apt/lists/*
ENV GOLANG_VERSION 1.6.3
ENV GOLANG_DOWNLOAD_URL https://golang.org/dl/go$GOLANG_VERSION.linux-amd64.tar.gz
ENV GOLANG_DOWNLOAD_SHA256 cdde5e08530c0579255d6153b08fdb3b8e47caabbe717bc7bcd7561275a87aeb
RUN curl -fsSL "$GOLANG_DOWNLOAD_URL" -o golang.tar.gz \
   && echo "$GOLANG_DOWNLOAD_SHA256  golang.tar.gz" ,  sha256sum -c - \
   && tar -C /usr/local -xzf golang.tar.gz \
   && rm golang.tar.gz
ENV GOPATH /go
ENV PATH $GOPATH/bin:/usr/local/go/bin:$PATH
RUN mkdir -p "$GOPATH/src" "$GOPATH/bin" && chmod -R 777 "$GOPATH"
WORKDIR $GOPATH
COPY go-wrapper /usr/local/bin/
```

下面，将讲解Dockerfile中各种指令的应用。

## 指令说明

Dockerfile中指令的一般格式为INSTRUCTION arguments，包括“配置指令”（配置镜像信息）和“操作指令”（具体执行操作），参见表：

![Dockerfile中的指令及说明](Docker使用Dockerfile创建镜像/Dockerfile中的指令及说明.png)

下面分别进行介绍。

### 配置指令

#### ARG

定义创建镜像过程中使用的变量。

格式为：

```bash
ARG <name>[=<default value>]
```

在执行docker build时，可以通过-build-arg\[=]来为变量赋值。当镜像编译成功后，ARG指定的变量将不再存在（ENV指定的变量将在镜像中保留）。

Docker内置了一些镜像创建变量，用户可以直接使用而无须声明，包括（不区分大小写）HTTP_PROXY、HTTPS_PROXY、FTP_PROXY、NO_PROXY。

#### FROM

指定所创建镜像的基础镜像。

格式为：

```bash
FROM <image> [AS <name>]
OR
FROM <image>:<tag> [AS <name>]
OR
FROM<image>@<digest> [AS <name>]。
```

任何Dockerfile中第一条指令必须为FROM指令。并且，如果在同一个Dockerfile中创建多个镜像时，可以使用多个FROM指令（每个镜像一次）。

为了保证镜像精简，可以选用体积较小的镜像如Alpine或Debian作为基础镜像。例如：

```bash
ARG VERSION=9.3
FROM debian:${VERSION}
```

#### LABEL

LABEL指令可以为生成的镜像添加元数据标签信息。这些信息可以用来辅助过滤出特定镜像。

格式为：

```bash
LABEL <key>=<value> <key>=<value> <key>=<value> ...
```

例如：

```bash
LABEL version="1.0.0-rc3"
LABEL author="yeasy@github" date="2020-01-01"
LABEL description="This text illustrates \
                   that label-values can span multiple lines."
```

#### EXPOSE

声明镜像内服务监听的端口。

格式为：

```bash
EXPOSE <port> [<port>/<protocol>...]
```

例如：

```bash
EXPOSE 22 808443
```

注意该指令只是起到声明作用，并不会自动完成端口映射。

如果要映射端口出来，在启动容器时可以使用-P参数（Docker主机会自动分配一个宿主机的临时端口）或-p HOST_PORT:CONTAINER_PORT参数（具体指定所映射的本地端口）。

#### ENV

指定环境变量，在镜像生成过程中会被后续RUN指令使用，在镜像启动的容器中也会存在。

格式为：

```bash
ENV <key> <value>
OR
ENV <key>=<value> ...
```

例如：

```bash
ENV APP_VERSION=1.0.0
ENV APP_HOME=/usr/local/app
ENV PATH $PATH:/usr/local/bin
```

指令指定的环境变量在运行时可以被覆盖掉，如：

```bash
docker run --env <key>=<value> built_image
```

注意当一条ENV指令中同时为多个环境变量赋值并且值也是从环境变量读取时，会为变量都赋值后再更新。如下面的指令，最终结果为key1=value1 key2=value2：

```bash
ENV key1=value2
ENV key1=value1 key2=${key1}
```

#### ENTRYPOINT

ENTRYPOINT指定镜像的默认入口命令，该入口命令会在启动容器时作为根命令执行，所有传入值作为该命令的参数。

支持两种格式：

- ENTRYPOINT ["executable", "param1", "param2"]：exec调用执行；
- ENTRYPOINT command param1 param2：shell中执行。

此时，CMD指令指定值将作为根命令的参数。

每个Dockerfile中只能有一个ENTRYPOINT，当指定多个时，只有最后一个起效。

在运行时，可以被--entrypoint参数覆盖掉，如：

```bash
docker run --entrypoint
```

#### VOLUME

创建一个数据卷挂载点。

格式为：

```bash
VOLUME ["/data"]
```

运行容器时可以从本地主机或其他容器挂载数据卷，一般用来存放数据库和需要保持的数据等。

#### USER

指定运行容器时的用户名或UID，后续的RUN等指令也会使用指定的用户身份。

格式为：

```bash
USER daemon
```

当服务不需要管理员权限时，可以通过该命令指定运行用户，并且可以在Dockerfile中创建所需要的用户。例如：

```bash
RUN groupadd -r postgres && useradd --no-log-init -r -g postgres postgres
```

要临时获取管理员权限可以使用gosu命令。

#### WORKDIR

为后续的RUN、CMD、ENTRYPOINT指令配置工作目录。

格式为：

```bash
WORKDIR /path/to/workdir
```

可以使用多个WORKDIR指令，后续命令如果参数是相对路径，则会基于之前命令指定的路径。例如：

```bash
WORKDIR /a
WORKDIR b
WORKDIR c
RUN pwd
```

则最终路径为/a/b/c。

因此，为了避免出错，推荐WORKDIR指令中只使用绝对路径。

#### ONBUILD

指定当基于所生成镜像创建子镜像时，自动执行的操作指令。

格式为：

```bash
ONBUILD [INSTRUCTION]
```

例如，使用如下的Dockerfile创建父镜像ParentImage，指定ONBUILD指令：

```bash
# Dockerfile for ParentImage
[...]
ONBUILD ADD . /app/src
ONBUILD RUN /usr/local/bin/python-build --dir /app/src
[...]
```

使用docker build命令创建子镜像ChildImage时（FROM ParentImage），会首先执行ParentImage中配置的ONBUILD指令：

```bash
# Dockerfile for ChildImage
FROM ParentImage
```

等价于在ChildImage的Dockerfile中添加了如下指令：

```bash
#Automatically run the following when building ChildImage
ADD . /app/src
RUN /usr/local/bin/python-build --dir /app/src
...
```

由于ONBUILD指令是隐式执行的，推荐在使用它的镜像标签中进行标注，例如ruby:2.1-onbuild。

ONBUILD指令在创建专门用于自动编译、检查等操作的基础镜像时，十分有用。

#### STOPSIGNAL

指定所创建镜像启动的容器接收退出的信号值：

```bash
STOPSIGNAL signal
```

#### HEALTHCHECK

配置所启动容器如何进行健康检查（如何判断健康与否），自Docker 1.12开始支持。

格式有两种：

- HEALTHCHECK \[OPTIONS] CMD command：根据所执行命令返回值是否为0来判断；
- HEALTHCHECK NONE：禁止基础镜像中的健康检查。

OPTION支持如下参数：

- -interval=DURATION(default: 30s)：过多久检查一次；
- -timeout=DURATION(default: 30s)：每次检查等待结果的超时；
- -retries=N(default: 3)：如果失败了，重试几次才最终确定失败。

#### SHELL

指定其他命令使用shell时的默认shell类型：

```bash
SHELL ["executable", "parameters"]
```

默认值为\["/bin/sh", "-c"]。

> 注意：对于Windows系统，建议在Dockerfile开头添加#escape=`来指定转义符。

### 操作指令

#### RUN

该命令的用处是运行指定命令。

格式为：

```bash
RUN <command>
RUN ["executable", "param1", "param2"]
```

注意后者指令会被解析为JSON数组，因此必须用双引号。前者默认将在shell终端中运行命令，即/bin/sh -c；后者则使用exec执行，不会启动shell环境。

指定使用其他终端类型可以通过第二种方式实现，例如RUN \["/bin/bash", "-c","echohello"]。

每条RUN指令将在当前镜像基础上执行指定命令，并提交为新的镜像层。当命令较长时可以使用\来换行。例如：

```bash
RUN apt-get update \
    && apt-get install -y libsnappy-dev zlib1g-dev libbz2-dev \
    && rm -rf /var/cache/apt \
    && rm -rf /var/lib/apt/lists/*
```

#### CMD

CMD指令用来指定启动容器时默认执行的命令。

支持三种格式：

- CMD \["executable", "param1", "param2"]：相当于执行executable param1param2，推荐方式；
- CMD command param1 param2：在默认的Shell中执行，提供给需要交互的应用；
- CMD \["param1", "param2"]：提供给ENTRYPOINT的默认参数。

每个Dockerfile只能有一条CMD命令。如果指定了多条命令，只有最后一条会被执行。

如果用户启动容器时候手动指定了运行的命令（作为run命令的参数），则会覆盖掉CMD指定的命令。

#### ADD

该命令用来添加内容到镜像。

格式为：

```bash
ADD <src> <dest>
```

该命令将复制指定的\<src>路径下内容到容器中的\<dest>路径下。

其中\<src>可以是Dockerfile所在目录的一个相对路径（文件或目录）；也可以是一个URL；还可以是一个tar文件（自动解压为目录）\<dest>可以是镜像内绝对路径，或者相对于工作目录（WORKDIR）的相对路径。

路径支持正则格式，例如：

```bash
ADD *.c /code/
```

#### COPY

复制内容到镜像。

格式为：

```bash
COPY <src> <dest>
```

复制本地主机的\<src>（为Dockerfile所在目录的相对路径，文件或目录）下内容到镜像中的\<dest>。目标路径不存在时，会自动创建。

路径同样支持正则格式。

COPY与ADD指令功能类似，当使用本地目录为源目录时，推荐使用COPY。

## 创建镜像

编写完成Dockerfile之后，可以通过docker \[image] build命令来创建镜像。

基本的格式为

```bash
docker build [OPTIONS] PATH | URL | -。
```

该命令将读取指定路径下（包括子目录）的Dockerfile，并将该路径下所有数据作为上下文（Context）发送给Docker服务端。Docker服务端在校验Dockerfile格式通过后，逐条执行其中定义的指令，碰到ADD、COPY和RUN指令会生成一层新的镜像。最终如果创建镜像成功，会返回最终镜像的ID。

如果上下文过大，会导致发送大量数据给服务端，延缓创建过程。因此除非是生成镜像所必需的文件，不然不要放到上下文路径下。如果使用非上下文路径下的Dockerfile，可以通过-f选项来指定其路径。

要指定生成镜像的标签信息，可以通过-t选项。该选项可以重复使用多次为镜像一次添加多个名称。

例如，上下文路径为/tmp/docker_builder/，并且希望生成镜像标签为builder/first_image:1.0.0，可以使用下面的命令：

```bash
$ docker build -t builder/first_image:1.0.0 /tmp/docker_builder/
```

### 命令选项

docker \[image] build命令支持一系列的选项，可以调整创建镜像过程的行为，参见表：

![创建镜像的命令选项及说明](Docker使用Dockerfile创建镜像/创建镜像的命令选项及说明.png)

### 选择父镜像

大部分情况下，生成新的镜像都需要通过FROM指令来指定父镜像。父镜像是生成镜像的基础，会直接影响到所生成镜像的大小和功能。

用户可以选择两种镜像作为父镜像，一种是所谓的基础镜像（baseimage），另外一种是普通的镜像（往往由第三方创建，基于基础镜像）。

基础镜像比较特殊，其Dockerfile中往往不存在FROM指令，或者基于scratch镜像（FROM scratch），这意味着其在整个镜像树中处于根的位置。

下面的Dockerfile定义了一个简单的基础镜像，将用户提前编译好的二进制可执行文件binary复制到镜像中，运行容器时执行binary命令：

```bash
FROM scratch
ADD binary /
CMD ["/binary"]
```

普通镜像也可以作为父镜像来使用，包括常见的busybox、debian、ubuntu等。

Docker不同类型镜像之间的继承关系如图所示：

![镜像的继承关系](Docker使用Dockerfile创建镜像/镜像的继承关系.png)

### 使用.dockerignore文件

可以通过.dockerignore文件（每一行添加一条匹配模式）来让Docker忽略匹配路径或文件，在创建镜像时候不将无关数据发送到服务端。

例如下面的例子中包括了6行忽略的模式（第一行为注释）：

```bash
# .dockerignore文件中可以定义忽略模式
*/temp*
*/*/temp*
tmp?
~*
Dockerfile
!README.md
```

.dockerignore文件中模式语法支持Golang风格的路径正则格式：

- “\*”表示任意多个字符；
- “\?”代表单个字符；
- “\!”表示不匹配（即不忽略指定的路径或文件）。

### 多步骤创建

自17.05版本开始，Docker支持多步骤镜像创建（Multi-stage build）特性，可以精简最终生成的镜像大小。

对于需要编译的应用（如C、Go或Java语言等）来说，通常情况下至少需要准备两个环境的Docker镜像：

- 编译环境镜像：包括完整的编译引擎、依赖库等，往往比较庞大。作用是编译应用为二进制文件；
- 运行环境镜像：利用编译好的二进制文件，运行应用，由于不需要编译环境，体积比较小。

使用多步骤创建，可以在保证最终生成的运行环境镜像保持精简的情况下，使用单一的Dockerfile，降低维护复杂度。

以Go语言应用为例。创建干净目录，进入到目录中，创建main.go文件，内容为：

```go
// main.go will output "Hello, Docker"

package main

import (
    "fmt"
)
func main() {
    fmt.Println("Hello, Docker")
}
```

创建Dockerfile，使用golang:1.9镜像编译应用二进制文件为app，使用精简的镜像alpine:latest作为运行环境。Dockerfile完整内容为：

```bash
# Define stage name as builder
FROM golang:1.9 as builder 
RUN mkdir -p /go/src/test
WORKDIR /go/src/test
COPY main.go .
RUN CGO_ENABLED=0 GOOS=linux go build -o app .

FROM alpine:latest
RUN apk --no-cache add ca-certificates
WORKDIR /root/
# Copy file from the builder stage
COPY --from=builder /go/src/test/app .
CMD ["./app"]
```

执行如下命令创建镜像，并运行应用：

```bash
$ docker build -t yeasy/test-multistage:latest .
Sending build context to Docker daemon  3.072kB
Step 1/10 : FROM golang:1.9 as builder
 ---> ef89ef5c42a9
Step 2/10 : RUN mkdir -p /go/src/test
 ---> Using cache
 ---> 55501dca455b
Step 3/10 : WORKDIR /go/src/test
 ---> Using cache
 ---> 37dc4353bc0b
Step 4/10 : COPY main.go .
 ---> Using cache
 ---> 0caaf30989bb
Step 5/10 : RUN CGO_ENABLED=0 GOOS=linux go build -o app .
 ---> Using cache
 ---> 748f7a19b959
Step 6/10 : FROM alpine:latest
 ---> e7d92cdc71fe
Step 7/10 : RUN apk --no-cache add ca-certificates
 ---> Running in add993e3f9a3
fetch http://dl-cdn.alpinelinux.org/alpine/v3.11/main/x86_64/APKINDEX.tar.gz
fetch http://dl-cdn.alpinelinux.org/alpine/v3.11/community/x86_64/APKINDEX.tar.gz
(1/1) Installing ca-certificates (20191127-r1)
Executing busybox-1.31.1-r9.trigger
Executing ca-certificates-20191127-r1.trigger
OK: 6 MiB in 15 packages
Removing intermediate container add993e3f9a3
 ---> caafef42f4ec
Step 8/10 : WORKDIR /root/
 ---> Running in b18725a9c691
Removing intermediate container b18725a9c691
 ---> 102c2d659e8e
Step 9/10 : COPY --from=builder /go/src/test/app .
 ---> 3198b228fd8f
Step 10/10 : CMD ["./app"]
 ---> Running in 5d862e606b52
Removing intermediate container 5d862e606b52
 ---> 83acac131949
Successfully built 83acac131949
Successfully tagged yeasy/test-multistage:latest


// 运行应用
[root@docker build_go]# docker run --rm yeasy/test-multistage:latest
Hello, Docker
```

查看生成的最终镜像，大小只有8MB：

```bash
$ docker images|grep test-multistage
```

## 最佳实践

所谓最佳实践，就是从需求出发，来定制适合自己、高效方便的镜像。

首先，要尽量吃透每个指令的含义和执行效果，多编写一些简单的例子进行测试，弄清楚了再撰写正式的Dockerfile。此外，Docker Hub官方仓库中提供了大量的优秀镜像和对应的Dockefile，可以通过阅读它们来学习如何撰写高效的Dockerfile。

在应用过程中，也总结了一些实践经验。建议读者在生成镜像过程中，尝试从如下角度进行思考，完善所生成镜像：

- 精简镜像用途：尽量让每个镜像的用途都比较集中单一，避免构造大而复杂、多功能的镜像；
- 选用合适的基础镜像：容器的核心是应用。选择过大的父镜像（如Ubuntu系统镜像）会造成最终生成应用镜像的臃肿，推荐选用瘦身过的应用镜像（如node:slim），或者较为小巧的系统镜像（如alpine、busybox或debian）；
- 提供注释和维护者信息：Dockerfile也是一种代码，需要考虑方便后续的扩展和他人的使用；
- 正确使用版本号：使用明确的版本号信息，如1.0，2.0，而非依赖于默认的latest。通过版本号可以避免环境不一致导致的问题；
- 减少镜像层数：如果希望所生成镜像的层数尽量少，则要尽量合并RUN、ADD和COPY指令。通常情况下，多个RUN指令可以合并为一条RUN指令；
- 恰当使用多步骤创建（17.05+版本支持）：通过多步骤创建，可以将编译和运行等过程分开，保证最终生成的镜像只包括运行应用所需要的最小化环境。当然，用户也可以通过分别构造编译镜像和运行镜像来达到类似的结果，但这种方式需要维护多个Dockerfile；
- 使用.dockerignore文件：使用它可以标记在执行docker build时忽略的路径和文件，避免发送不必要的数据内容，从而加快整个镜像创建过程。
- 及时删除临时文件和缓存文件：特别是在执行apt-get指令后，/var/cache/apt下面会缓存了一些安装包；
- 提高生成速度：如合理使用cache，减少内容目录下的文件，或使用．dockerignore文件指定等；
- 调整合理的指令顺序：在开启cache的情况下，内容不变的指令尽量放在前面，这样可以尽量复用；
- 减少外部源的干扰：如果确实要从外部引入数据，需要指定持久的地址，并带版本信息等，让他人可以复用而不出错。