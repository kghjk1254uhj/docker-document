<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
**Table of Contents**  *generated with [DocToc](https://github.com/thlorenz/doctoc)*

- [Docker设置自定义IP和hostname](#docker%E8%AE%BE%E7%BD%AE%E8%87%AA%E5%AE%9A%E4%B9%89ip%E5%92%8Chostname)
  - [创建自定义IP](#%E5%88%9B%E5%BB%BA%E8%87%AA%E5%AE%9A%E4%B9%89ip)
  - [使用新的网络类型创建并启动一个容器](#%E4%BD%BF%E7%94%A8%E6%96%B0%E7%9A%84%E7%BD%91%E7%BB%9C%E7%B1%BB%E5%9E%8B%E5%88%9B%E5%BB%BA%E5%B9%B6%E5%90%AF%E5%8A%A8%E4%B8%80%E4%B8%AA%E5%AE%B9%E5%99%A8)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

# Docker设置自定义IP和hostname

比如要实现：

![模型图](Docker设置自定义IP和hostname/模型图.png)

Docker安装之后，默认会创建三种网络类型，bridge、host和none，可通过如下命令查看：

```bash
[root@docker ~]# docker network ls
NETWORK ID          NAME                DRIVER              SCOPE
44cf614154fa        bridge              bridge              local
4f31fb4c4d02        host                host                local
55e4a7003b8e        none                null                local
```

bridge：网络桥接

默认情况下启动、创建容器都是用该模式，所以每次docker容器重启时会按照顺序获取对应ip地址，这就导致容器每次重启，IP都发生变化。

none：无指定网络

启动容器时，可以通过–network=none，Docker容器不会分配局域网IP。

host：主机网络

Docker容器的网络会附属在主机上，两者是互通的。

而我们想自定义一个IP地址，则需要新建一个bridge，下面来介绍创建方法：

## 创建自定义IP

```bash
$ docker network create --subnet=192.168.218.111/16 mynetwork
```

![创建自定义的网络类型](Docker设置自定义IP和hostname/创建自定义的网络类型.png)

## 使用新的网络类型创建并启动一个容器

```bash
$ docker run -itd --net mynetwork --privileged=true --ip 192.168.218.212 --hostname="slaves2.org.cn" --add-host "master.org.cn master:192.168.218.221" --add-host "slaves1.org.cn slaves1:192.168.218.222" --name test_net centos:hadoop /bin/bash

[root@docker ~]# docker exec -it test_net /bin/bash

[root@slaves2 /]# cat /etc/hosts
127.0.0.1       localhost
::1     localhost ip6-localhost ip6-loopback
fe00::0 ip6-localnet
ff00::0 ip6-mcastprefix
ff02::1 ip6-allnodes
ff02::2 ip6-allrouters
192.168.218.221 master.org.cn master
192.168.218.222 slaves1.org.cn slaves1
192.168.218.212 slaves2.org.cn slaves2
```